# Generates a list of 10 random numbers between -19 and 19 included,
# and determines the two largest (distinct) strictly negative and
# the two smallest (distinct) strictly positive elements in the list.
#
# Writtten by Pang Luo and Eric Martin for COMP9021


import sys
from random import seed, randint


nb_of_elements = 10

if len(sys.argv) != 2:
    print('Provide one and only only command line argument')
    sys.exit()
try:
    seed(int(sys.argv[1]))
except:
    print('The command line argument should be an integer.')
    sys.exit()

L = [None] * nb_of_elements
for i in range(nb_of_elements):
    L[i] = randint(-19, 19)

print('The generated list is:', L)

first_max_negative_element = None
second_max_negative_element = None
first_min_positive_element = None
second_min_positive_element = None

positive_list = [element for element in L if element > 0]
negative_list = [element for element in L if element < 0]
positive_list.sort(reverse=True)
negative_list.sort()

if positive_list:
    first_min_positive_element = positive_list.pop()
    while positive_list:
        tmp = positive_list.pop()
        if tmp != first_min_positive_element:
            second_min_positive_element = tmp
            break

if negative_list:
    first_max_negative_element = negative_list.pop()
    while negative_list:
        tmp = negative_list.pop()
        if tmp != first_max_negative_element:
            second_max_negative_element = tmp
            break

if not first_max_negative_element:
    print('There is no stricly negative integer.')
else:
    if not second_max_negative_element:
        print('There is only one strictly negative integer, {:}.'.format(first_max_negative_element))
    else:
        print('The largest and second largest strictly negative integers are ' +
              '{:} and {:}.'.format(first_max_negative_element, second_max_negative_element))
if not first_min_positive_element:
    print('There is no stricly positive integer.')
else:
    if not second_min_positive_element:
        print('There is only one strictly positive integer, {:}.'.format(first_min_positive_element))
    else:
        print('The smallest and second smallest strictly positive integers are ' +
              '{:} and {:}.'.format(first_min_positive_element, second_min_positive_element))
                
    


    
    
