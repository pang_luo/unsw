# Randomly fills a grid of size 10 x 10 with 0s and 1s,
# in an estimated proportion of 1/2 for each
# and computes the longest leftmost path that starts
# from the top left corner -- a path
# consisting of horizontally or vertically adjacent 1s --,
# visiting every point on the path once only.
#
# Written by Pang Luo and Eric Martin for COMP9021


import sys
from random import seed, randint
from array_queue import *


dim = 10
grid = [[0] * dim for i in range(dim)]

def display_grid():
    for i in range(dim):
        print('    ', end = '')
        for j in range(dim):
            print(' ', grid[i][j], end = '')
        print()
    print()

            
def leftmost_longest_path_from_top_left_corner():
    if grid[0][0] == 0:
        return []
    pathQueue = ArrayQueue()
    pathQueue.enqueue([-1, 0])
    while pathQueue:
        front = pathQueue.dequeue()
        possibleNexts = [(front[-1] - front[-2]) * vector + front[-1]
                         for vector in (-1j, 1, 1j)]
        for pos in possibleNexts:
            if (pos not in front and
                0 <= pos.real <= 9 and -9 <= pos.imag <= 0 and
                grid[abs(int(pos.imag))][int(pos.real)]):
                pathQueue.enqueue(front + [pos])
    return [(abs(int(pos.imag)), int(pos.real)) for pos in front[1:]]
                
                
provided_input = input('Enter one integer: ')
try:
    seed_arg = int(provided_input)
except:
    print('Incorrect input, giving up.')
    sys.exit()
    
seed(seed_arg)
# We fill the grid with randomly generated 0s and 1s,
# with for every cell, a probability of 1/2 to generate a 0.
for i in range(dim):
    for j in range(dim):
        grid[i][j] = randint(0, 1)
print('Here is the grid that has been generated:')
display_grid()

path = leftmost_longest_path_from_top_left_corner()
if not path:
    print('There is no path from the top left corner')
else:
    print('The leftmost longest path from the top left corner is {:})'.format(path))
           
