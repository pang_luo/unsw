# Uses National Data on the relative frequency of given names in the population of U.S. births,
# stored in a directory "names", in files named "yobxxxx.txt with xxxx (the year of birth)
# ranging from 1880 to2013.
# Prompts the user for a female first name, and finds out the years when this name was most popular
# in terms of ranking. Displays the ranking, and the years in decreasing order of frequency.
# 
# Written by Pang Luo and Eric Martin for COMP9021


import sys
import os

targeted_first_name = input('Enter a female first name: ')
rank = float('inf')

best_years = []

import re

directory = 'names'
patternObj = re.compile(r'^yob(\d{4})\.txt$')
    
for fileName in os.listdir(directory):
    match = re.search(patternObj, fileName) 
    if match:
        try:
            fileHandle = open(os.path.join(directory, fileName))
        except:
            pass
        else:
            nLine = 0
            totalFemale = 0
            change = False
            for line in fileHandle:
                nLine += 1
                lineList = line.split(',')
                if lineList[1].strip() != 'F':
                    break
                totalFemale += int(lineList[2].strip())
                if targeted_first_name == lineList[0].strip():
                    if nLine > rank:
                        break
                    change = True
                    if nLine < rank:
                        rank = nLine
                        best_years = [[int(lineList[2].strip()),
                                          int(match.group(1))]]                       
                    else:
                        best_years.append(
                            [int(lineList[2].strip()), int(match.group(1))])
            fileHandle.close()
            if change:
                best_years[-1][0] /= totalFemale

best_years.sort(reverse=True)
best_years = [element[1] for element in best_years]

if not best_years:
    print('{:} is not a female first name in my records.'.format(targeted_first_name))
else:
    print('By decreasing order of frequency, {:} was most popular in the years: '.format(targeted_first_name), end = '')
    for year in best_years:
        print(year, end = ' ')
    print('\nIts rank was {:} then.'.format(rank))
