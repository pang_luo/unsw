# A peg solitaire simulation
#
# Written by Pang Luo
# Last modified on 10/05/2015

class Solitaire:
    def __init__(self):
        self.configuration = []
        self._allowed = (set(range(13, 16)) | set(range(22, 27)) |
                        set(range(31, 38)) | set(range(41, 48)) |
                        set(range(51, 58)) | set(range(62, 67)) |
                        set(range(73, 76)))
        self._jumps = []
        self._visited = set()
        self._set_jump_weight()

    def _set_jump_weight(self):
        """
        To give a weight to every legal jump. A jump with a higher weight
        has a higher chance to be chosen during the pruning process.
        """
        posWeight = {}
        badPosTuple = (15, 26, 37, 57, 66, 75, 73, 62, 51, 31, 22, 13)
        self._jumpWeight = {}

        for pos in self._allowed:
            if pos in badPosTuple:
                posWeight[pos] = -1
            elif (pos // 10 - pos % 10) % 2:
                posWeight[pos] = 1
            else:
                posWeight[pos] = 0
            
        for endPos in self._allowed:
            for startPos in (endPos-20, endPos+20, endPos-2, endPos+2):
                try:
                    self._jumpWeight[(startPos, endPos)] = (
                        posWeight[endPos] - posWeight[startPos] -
                        posWeight[(startPos + endPos) // 2])
                except:
                    pass

    @staticmethod
    def _sort_pos(pos):
        return pos % 10 * 10 - pos // 10

    def get_configuration(self):
        return sorted(self.configuration, key=self._sort_pos, reverse=True)

    def get_configuration_length(self):
        return len(self.configuration)

    def configuration_is(self, configuration):
        return set(self.configuration) == set(configuration)

    def set_configuration(self, configuration):
        if set(configuration) <= self._allowed:
            # set is necessary to eliminate duplicates
            self.configuration = list(set(configuration))
            self.configuration.sort(key=self._sort_pos, reverse=True)
        else:
            print('Invalid configuration.')

    def display_configuration(self):
        for i in range(7, 0, -1):
            for j in range(1, 8):
                pos = j * 10 + i
                if pos in self._allowed:
                    if (j * 10 + i) in self.configuration:
                        print('X', end=' ')
                    else:
                        print('O', end=' ')
                else:
                    print(' ', end=' ')
            print()

    def set_complement_configuration(self, configuration):
        self.set_configuration(list(self._allowed - set(configuration)))
    
    def _apply_jump(self, jump, order=True):
        self.configuration.remove(jump[0])
        self.configuration.remove((jump[0] + jump[1]) // 2)
        self.configuration.append(jump[1])
        if order:
            self.configuration.sort(key=self._sort_pos, reverse=True)
                          
    def apply_jumps(self, jumps):
        if not jumps:
            return
        originalConfiguration = self.configuration[:]
        for jump in jumps:
            gap = abs(jump[0] - jump[1])
            if ((gap == 20 or gap == 2) and jump[0] in self.configuration
                and jump[1] in (self._allowed - set(self.configuration))
                and (jump[0]+jump[1]) // 2 in self.configuration):
                self._apply_jump(jump)
            else:
                self.configuration = originalConfiguration[:]
                print('Invalid sequence of jumps.')
                return

    def display_jumps(self, jumps):
        for jump in jumps:
            self._apply_jump(jump)
            self.display_configuration()
            print()

    def _apply_reverse_jump(self, jump, order=True):
        self.configuration.remove(jump[1])
        self.configuration.append(jump[0])
        self.configuration.append((jump[0]+jump[1]) // 2)
        if order:
            self.configuration.sort(key=self._sort_pos, reverse=True)

    def apply_reverse_jumps(self, jumps):
        if not jumps:
            return
        originalConfiguration = self.configuration[:]
        for jump in jumps[::-1]:
            gap = abs(jump[0] - jump[1])
            vacantPos = self._allowed - set(self.configuration)
            if ((gap == 20 or gap == 2) and jump[1] in self.configuration
                and jump[0] in vacantPos
                and (jump[0]+jump[1]) // 2 in vacantPos):
                self._apply_reverse_jump(jump)
            else:
                self.configuration = originalConfiguration[:]
                print('Invalid sequence of jumps.')
                return
                
    def display_reverse_jumps(self, jumps):
        for jump in jumps[::-1]:
            self._apply_reverse_jump(jump)
            self.display_configuration()
            print()
                
    def list_possible_next_jump(self):
        jumps = []
        for endPos in (self._allowed - set(self.configuration)):
            for startPos in (endPos-20, endPos+20, endPos-2, endPos+2):
                if (startPos in self.configuration and
                    (startPos + endPos) // 2 in self.configuration):
                    jumps.append([startPos, endPos])
        # For jumps with different start positions, the start positions
        #     determine the order.
        # For jumps with the same start position, the end positions
        #     determine the order.
        # To reflect the dominance of start positions over end positions,
        #     '* 100' was included in the lambda function.
        jumps.sort(key=lambda x: (x[0] % 10 * 10 - x[0] // 10) * 100
                   + x[1] % 10 * 10 - x[1] // 10, reverse=True)
        return jumps
    
    def _get_pagoda(self, configuration):
        """To get the pagoda value (p1) of the current configuration so that
        it can be compared with the counterpart (p2) of the goal. If p1 < p2,
        this configuration can't lead to the goal and can be ignored.
        """
        pagoda = 0
        for pos in configuration:
            if pos in (37, 15, 13, 31, 51, 73, 75, 57):
                pagoda -= 1
            elif pos in (36, 46, 56, 25, 45, 65, 24, 34, 54, 64, 23, 43, 63,
                         32, 42, 52):
                pagoda += 1
        return pagoda

    def _get_parity(self, configuration):
        """To get the parity (p1) of the current configuration so that it can
        be compared with the counterpart (p2) of the goal. If any value in p1
        is smaller than the corresponding one in p2, this configuration
        can't lead to the goal and can be ignored.
        """
        parity = {'00': 0, '01': 0, '10': 0, '11': 0}
        for pos in configuration:
            parity[str(pos // 10 % 2) + str(pos % 10 % 2)] += 1
        return parity

    def _valid_parity(self, goalParity):
        currParity = self._get_parity(self.configuration)
        for key in goalParity:
            if currParity[key] < goalParity[key]:
                return False
        return True

    def generated_sequence_of_jumps(self, goal=[]):
        # The configuration of which the length is more than threshold is
        #     supposed to be "dense" enough so that pruning can be used;
        #     otherwise brute force is applied to calculate every possibility.
        threshold = max(len(goal) + 3, 9) + \
                      (36 - self.get_configuration_length()) // 3
        offset = self.get_configuration_length() % 6
        if goal:
            pagoda = self._get_pagoda(goal)
            parity = self._get_parity(goal)
            jumps = self._generated_sequence_with_goal(
                goal, threshold, offset, pagoda, parity)
        else:
            jumps = self._generated_sequence_without_goal(threshold, offset)
        self._jumps = []
        self._visited.clear()
        return jumps

    def _filter_jumps(self, threshold, offset):
        jumps = self.list_possible_next_jump()
        jumps.sort(key=lambda x: self._jumpWeight[tuple(x)], reverse=True)

        # If the configuration is "dense" enough, we prune the tree
        #     moderately. Once the configuration becomes "sparing", brute
        #     force is used.
        if self.get_configuration_length() > threshold:
            if (self.get_configuration_length() - offset) % 6 == 0:
                jumps = jumps[:3]
            elif (self.get_configuration_length() - offset) % 3 == 0:
                jumps = jumps[:2]
            elif (self.get_configuration_length() - offset) % 2 == 0:
                jumps = jumps[:2]
            else:
                jumps = jumps[:1]
        return jumps
        
    def _generated_sequence_without_goal(self, threshold, offset):
        originalConfiguration = self.configuration[:]
        longestJumps = []
        qualifiedNextJumps = self._filter_jumps(threshold, offset)

        for jump in qualifiedNextJumps:
            self._apply_jump(jump)
            if tuple(self.configuration) in self._visited:
                self.configuration = originalConfiguration[:]
                continue
            self._visited.add(tuple(self.configuration))
            jumps = [jump] + self._generated_sequence_without_goal(
                threshold, offset)
            if len(jumps) > len(longestJumps):
                longestJumps = jumps
            self.configuration = originalConfiguration[:]
            if len(longestJumps) == len(originalConfiguration) - 1:
                break
        return longestJumps
        
    def _generated_sequence_with_goal(self, goal, threshold,
                                      offset, pagoda, parity):
        if set(self.configuration) == set(goal):
            return self._jumps[:]
        if (self.get_configuration_length() <= len(goal) or
            self._get_pagoda(self.configuration) < pagoda or
            not self._valid_parity(parity)):
            return
        originalConfiguration = self.configuration[:]
        qualifiedNextJumps = self._filter_jumps(threshold, offset)

        for jump in qualifiedNextJumps:
            self._apply_jump(jump)
            if tuple(self.configuration) in self._visited:
                self.configuration = originalConfiguration[:]              
                continue
            self._jumps.append(jump)
            self._visited.add(tuple(self.configuration))
            jumps = self._generated_sequence_with_goal(
                goal, threshold, offset, pagoda, parity)
            self.configuration = originalConfiguration[:]
            self._jumps.pop()
            if jumps:
                return jumps
        return None

            





