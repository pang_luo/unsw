# A S.A.T. maze generation program
# To generate a maze, we first draw the vertical lines and then the horizontal
#     lines within the north triangle. Once finished, we add other ordinary
#     lines. At last, we format all lines and write them to a file.
#     
# Written by Pang Luo
# Last modified on 28/05/2015

class Maze:
    def __init__(self, seq):
        if not isinstance(seq, str):
            print('The input should be a string.')
            return

        seq = seq.split()
        try:
            seq = [int(element) for element in seq]
            self.seq = seq
            self._nested = {}
        except:
            print('The input should consist of numbers.')
        else:
            if not 4 <= len(seq) <= 20:
                print('The input should consist of between 4 and 20 numbers.')
            elif set(seq) != set(range(len(seq))):
                print('The input should consist of 0, 1, 2... N, '
                      'in some order.')
            elif seq[0] != 0 or seq[-1] != len(seq) - 1:
                print('The input should start with 0 and '
                      'end with the largest number.')
            elif not self._is_alternating():
                print('The input should alternate '
                      'between even and odd numbers.')
            elif self._is_overlapping():
                print('The input defines overlapping pairs.')
            else:
                self._set_helper_var()


    def _set_helper_var(self):
        seqTail = self.seq[-1]
        if seqTail % 2:
            # self._vLinePivot: the middle vertical line or
            #     one of the middle vertical lines of the maze
            self._vLinePivot = ((seqTail, 0),
                                (seqTail, seqTail - 1))
            self._mazeWidth = 2 * seqTail
            self._mazeHeight = 2 * seqTail - 1
        else:
            self._vLinePivot = ((seqTail - 1, 0),
                               (seqTail - 1, seqTail - 1))
            self._mazeWidth = 2 * seqTail - 1
            self._mazeHeight = 2 * seqTail - 2

        self._xPivot = self._vLinePivot[0][0]
                
                           
    def _is_alternating(self):
        seq = self.seq
        for index in range(len(seq)):
            if (seq[index] - index) % 2:
                return False
        return True
    

    def _is_pairs_overlapping(self, pairA, pairB):
        """Judge if two pairs are overlapping, and calculate the nested depth
        of every pair. Every pair represents a vertical line and the nested
        depth is for its distance from the pivot line.
        """
        a, b = sorted(pairA)
        c, d = sorted(pairB)
        
        if a < c < b < d or c < a < d < b:
            return True

        nested = self._nested
        if a < c < d < b:    # nested
            nested[tuple(pairB)] += 1
        elif c < a < b < d:    # nested
            nested[tuple(pairA)] += 1
             
        return False


    def _is_overlapping(self):
        """For convenience, when judging if the sequence is overlapping,
        we initialize the nested depth of every pair in this function and
        process this depth in the method _is_pairs_overlapping.
        """
        seq = self.seq
        for i in range(len(seq) - 1):
            self._nested.setdefault(tuple(seq[i:i+2]), 0)

        for i in range(len(seq) - 1):
            for j in range(i + 2, len(seq) - 1, 2):
                if self._is_pairs_overlapping(
                    seq[i:i+2], seq[j:j+2]):
                    return True
                                           
        return False


    def _generate_special_vLines(self):
        vLinesLeft = []
        vLinesRight = []
        xPivot = self._xPivot
        nested = self._nested

        for element in nested:
            if element[0] % 2:
                vLinesRight.append(
                    ((xPivot + 1 + nested[element], min(element)),
                     (xPivot + 1 + nested[element], max(element) - 1)))
            else:
                vLinesLeft.append(
                    ((xPivot - 1 - nested[element], min(element)),
                     (xPivot - 1 - nested[element], max(element) - 1)))
        
        return vLinesLeft, vLinesRight


    def _generate_special_hLines(self, vLinesLeft, vLinesRight):
        hLines = []
        xPivot = self._xPivot
        mazeWidth = self._mazeWidth
        vLinesLeft.sort(key=lambda x: x[0][0])
        vLinesRight.sort(key=lambda x: x[0][0], reverse=True)

        # From every point on the top diagonal lines, we draw a horizontal
        #     line until it intersects with a vertical line or else reaches
        #     the pivot line.
        for yPos in range(self.seq[-1]):
            for vLine in vLinesLeft:
                if vLine[0][1] <= yPos <= vLine[1][1]:
                    hLines.append(
                        ((yPos, yPos),
                         (vLine[0][0], yPos)))
                    break
            else:
                hLines.append(
                    ((yPos, yPos),
                     (xPivot, yPos)))

            for vLine in vLinesRight:
                if vLine[0][1] <= yPos <= vLine[1][1]:
                    hLines.append(
                        ((vLine[0][0], yPos),
                         (mazeWidth - yPos, yPos)))
                    break
            else:
                hLines.append(
                    ((xPivot, yPos),
                     (mazeWidth - yPos, yPos)))
                
        return hLines


    def _get_lines_formatted(self, lines):
        lines.sort()
        linesFormatted = []
        i = 0
        while i < len(lines):
            if lines[i][0] != lines[i][1]:
                sentinel = i
                while (sentinel + 1 < len(lines) and
                       lines[sentinel][1] == lines[sentinel+1][0]):
                    sentinel += 1
                linesFormatted.append((lines[i][0], lines[sentinel][1]))
                i = sentinel + 1
            else:
                i += 1
        return linesFormatted
                

    def _write_to_file(self, hLinesFormatted, vLinesFormatted, fileName):
        templateHead = r'''\documentclass[10pt]{article}
\usepackage{tikz}
\pagestyle{empty}

\begin{document}

\vspace*{\fill}
\begin{center}
\begin{tikzpicture}[scale = 1.5, x = 0.25cm, y = -0.25cm, thick, purple]
'''

        templateTail = r'''\end{tikzpicture}
\end{center}
\vspace*{\fill}

\end{document}
'''

        templateBody = []
        templateBody.append('% Horizontal lines\n')
        
        for hLine in hLinesFormatted:
            templateBody.append('\draw' + str(tuple(hLine[0])) + ' -- ' +
                                str(tuple(hLine[1])) + ';\n')

        templateBody.append('% Vertical lines\n')
        for vLine in vLinesFormatted:
            templateBody.append('\draw' + str(tuple(vLine[0])) + ' -- ' +
                                str(tuple(vLine[1])) + ';\n')

        fileHandle = open(fileName + '.tex', 'w')
        fileHandle.write(templateHead + ''.join(templateBody) + templateTail)
        fileHandle.close()


    def generate_latex_code(self, fileName):
        vLinesLeft, vLinesRight = self._generate_special_vLines()
        hLines = self._generate_special_hLines(vLinesLeft, vLinesRight)

        vLines = [self._vLinePivot]
        vLines.extend(vLinesLeft)
        vLines.extend(vLinesRight)
        mazeHeight = self._mazeHeight
        mazeWidth = self._mazeWidth
            
        for xPos in range(self._xPivot):
            vLines.append(((xPos, xPos), (xPos, mazeHeight - xPos)))
            vLines.append(((mazeWidth - xPos, xPos),
                           (mazeWidth - xPos, mazeHeight - xPos)))

        for xPos in range(self.seq[-1]):
            hLines.append(((xPos, mazeHeight - xPos),
                           (mazeWidth - xPos, mazeHeight - xPos)))

        vLinesFormatted = self._get_lines_formatted(vLines)
        hLinesFormatted = self._get_lines_formatted(hLines)

        self._write_to_file(hLinesFormatted, vLinesFormatted, fileName)
                                          
