# Simulates the roll of 5 poker dice at most three times, as well as a
# given number of rolls of the 5 dice to evaluate the probabilities of
# the various hands.
#
# Written by Pang Luo
# Last modified on 05/04/2015

from copy import copy
from random import randint

# Each player has a total of 3 rolls
nRoll = 3

# There are 5 dice in total
nDice = 5

# Every die has 6 faces
nFace = 6

hands = ['Five of a kind', 'Four of a kind', 'Full house', 'Straight',
         'Three of a kind', 'Two pair', 'One pair', 'Bust']
dieTable = ['Ace', 'King', 'Queen', 'Jack', '10', '9']


def get_hand(rollDict):
    '''Returns the hand type.
    Keys of rollDict only consist of die faces that appear in a roll.
    Values are corresponding times they appear in a roll.
    For example, {3: 5} is for 'J J J J J', and {0: 1, 1: 1, 2: 1, 3: 1, 4: 1}
    is for 'A K Q J 10'.

    >>> get_hand({3: 5})
    'Five of a kind'

    >>> get_hand({0: 1, 1: 1, 2: 1, 3: 1, 4: 1})
    'Straight'
    '''
       
    if len(rollDict) == 1:           
        hand = 'Five of a kind'       
    elif len(rollDict) == 2:
        if max(rollDict.values()) == 4:
            hand = 'Four of a kind'
        else:
            hand = 'Full house'      
    elif len(rollDict) == 3:
        if max(rollDict.values()) == 3:
            hand = 'Three of a kind'
        else:
            hand = 'Two pair'         
    elif len(rollDict) == 4:
        hand = 'One pair'            
    elif 0 in rollDict and 5 in rollDict:
        hand = 'Bust'
    else:
        hand = 'Straight'
      
    return hand


def get_dice(chosenDice=''):
    '''Rolls based on the kept dice and returns faces in the form of dict.
    '''
    chosenDice = chosenDice.split()
    chosenDice = [dieTable.index(die) for die in chosenDice]
    rollDict = {}
    for die in chosenDice:
        try:
            rollDict[die] += 1
        except KeyError:
            rollDict[die] = 1
        
    for i in range(nDice - len(chosenDice)):
        try:
            # we use randint(0, 5) with 0, 1, 2, 3, 4 and 5
            # corresponding to Ace, King, Queen, Jack, 10 and 9, respectively.
            die = randint(0, nFace - 1)
            rollDict[die] += 1
        except KeyError:
            rollDict[die] = 1

    return rollDict


def get_status(chosenDice, allDice):
    '''Judges if the user input is legal and returns the status

    >>> get_status('all', ['Ace', 'Ace', 'Ace', 'King', 'King'])
    'all'

    >>> get_status('ace', ['Ace', 'Ace', 'Ace', 'King', 'King'])
    'mistake'

    >>> get_status('Ace King', ['Ace', 'Ace', 'Ace', 'King', 'King'])
    'part'

    >>> get_status('Ace Ace Ace King King', ['Ace', 'Ace', 'Ace', 'King', 'King'])
    'all'
    
    '''
    chosenDice = chosenDice.strip()
    if chosenDice == 'all' or chosenDice == 'All':
        status = 'all'
    else:
        allDiceCopy = copy(allDice)
        chosenDice = chosenDice.split()
        try:
            for die in chosenDice:
                allDiceCopy.remove(die)
        except ValueError:      
            status = 'mistake'
        else:
            if allDiceCopy:
                status = 'part'
            else:
                status = 'all'

    return status
            
            
def play():
    '''Simulates the interactive playing procedure'''
    
    roundTable = ['second', 'third']
    chosenDice = ''  # Dice which the user wants to keep, e.g. 'King Queen Queen'.
    for i in range(nRoll):
        dieList = []  # All the die faces in a roll, e.g. ['Ace', 'Ace', 'King', '10', '9'].
        print('The roll is:', end=' ')
        rollDict = get_dice(chosenDice)
        hand = get_hand(rollDict)
        for die, count in rollDict.items():
            for j in range(count):
                print(dieTable[die], end=' ')
                dieList.append(dieTable[die])
        print('\nIt is a', hand)
        if i == 2:
            break
        
        while True:
            chosenDice = input(
                'Which dice do you want to keep for the %s roll? '
                % roundTable[i])
            status = get_status(chosenDice, dieList)
            if status == 'all':
                print('Ok, done.')
                return
            elif status == 'part':
                break
            else:
                print('That is not possible, try again!')
        
                
def simulate(times):
    '''Simulates the game for designated times with interacting with the player'''
    sumDict = {'Five of a kind': 0, 'Four of a kind': 0, 'Full house': 0,
               'Straight': 0, 'Three of a kind': 0, 'Two pair': 0,
               'One pair': 0, 'Bust': 0}  # Values: times

    for i in range(times):
        rollDict = get_dice()
        hand = get_hand(rollDict)
        sumDict[hand] += 1
        
    maxLen = max(len(hand) for hand in hands)

    for hand in hands[:-1]:
        print('{h:<{width}}: {probability:.2%}'.format(
            h=hand, width=maxLen, probability=sumDict[hand]/times))


        
            
                
