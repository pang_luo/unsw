# Solves a multiplication based on the requirements.
#
# Written by Pang Luo
# Last modified on 26/03/2015


def distinct_digits(num):
    '''Verify if the num has distinct digits
    >>> distinct_digits(1234)
    True
    >>> distinct_digits(1231)
    False
    '''
    string = str(num)
    lenString = len(string)
    digitSet = set()
    for i in range(lenString):
        digitSet.add(string[i])

    if len(digitSet) == lenString:
        return True
    else:
        return False


def sum_digits(num):
    '''Calculate the sum of all digits of a num'''
    string = str(num)
    lenString = len(string)
    total = 0
    for i in range(lenString):
        total += int(string[i])
    return total


def print_result(resultList):
    print(' ' * 6, end='')
    # Print the multiplicand
    for i in str(resultList[0]):
        print(i, end=' ')

    print('\n    x', end=' ')
    # Print the multiplier
    for i in str(resultList[1]):
        print(i, end=' ')

    print('\n' + ' ' * 6 + '-' * 5)

    # Print three productParts
    for i in range(3):
        print(' ' * (4- 2 * i), end='')
        for i in str(resultList[3 + i]):
            print(i, end=' ')
        print('')

    print('-' * 11)

    # Print the product
    for i in str(resultList[2]):
        print(i, end=' ')

    print('\n')
    

lenMultiplier = 3

# Every productPart(3 in total) is at least 1000, since the leftmost star can't be zero.
productPartLimit = 1000

# The limit of multiplicand and multiplier
upperLimit = 999
lowerLimit = 100

# All the candidate numbers that have three distinct digits
numList = [num for num in range(lowerLimit, upperLimit + 1)
           if distinct_digits(num)]

for multiplicand in numList:
    for multiplier in numList:
        sumDigits = sum_digits(multiplicand)
        if sum_digits(multiplier) == sumDigits:
            product = multiplicand * multiplier
            if distinct_digits(product) and sum_digits(product) == sumDigits:
                strMultiplier = str(multiplier)[::-1] # Reversing for the convenience of output 
                resultList = [multiplicand, multiplier, product]
                for i in range(lenMultiplier):
                    productPart = multiplicand * int(strMultiplier[i])
                    if not (productPart > productPartLimit and
                            distinct_digits(productPart) and
                            sum_digits(productPart) == sumDigits):
                        break
                    resultList.append(productPart)
                # If there is no break in the for loop, print the result
                # otherwise the candidate hasn't met the demands.
                else:
                    print_result(resultList)
