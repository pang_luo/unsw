# Prompts the user for a nonnegative integer N and
# displays the first N + 1 rows of Pascal triangle.
#
# Written by Pang Luo
# Last modified on 26/03/2015


def get_nblanks():
    '''Returns the indentation between two adjacent lines, which is equal
    to the digit length of the biggest number in the triangle.
    For example, a triangle with 11 lines has 252 as the biggest number,
    so the indentation between two adjacent lines is 3.
    We can apply the combination formula to calculate the biggest number,
    which is C(nLines - 1, (nLines - 1) // 2).
    '''

    # pA corresponds to n(n-1)(n-2)...(n-r+1) in the combination formula
    pA = 1
    num = nLines - 1
    for i in range((nLines - 1) // 2):
        pA *= num
        num -= 1

    # pB corresponds to r! in the combination formula
    pB = 1
    for i in range(1, (nLines - 1) // 2 + 1):
        pB *= i

    return len(str(pA // pB))


def print_line(line, i):
    print(' ' * (nBlanks * (nLines - i) - 1), end='')
    for num in line:
        print('{n:<{width}}'.format(
            n=num, width=2*nBlanks), end='')
    print('')
    
    
def create_triangle():
    preLine = []
    currLine = []
    for i in range(nLines):
        currLine.append(1)
        for j in range(1, i):
            currLine.append(preLine[j - 1] + preLine[j])
        # Except line 0, every line should have another 1 as the end.
        if i:
            currLine.append(1)
        print_line(currLine, i)
        preLine = currLine
        currLine = []
        
    
while True:
    try:
        nLines = int(input('Enter a nonnegative integer: '))
        if nLines >= 0:
            nLines += 1
            break
    except ValueError:
        pass

# nBlanks: A global variable meaning indentation between two adjacent lines
# (equal to the digit length of the biggest number in the triangle)
nBlanks = get_nblanks()
create_triangle()






