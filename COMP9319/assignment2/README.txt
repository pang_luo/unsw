* Index File Design and Implementation Mechanism for Assignment 2
* Written by Pang Luo
* Last Modified on 25/04/2016

This program involves two mechanisms, one of which is memory based and the other index based. Depending on the size of the input file, one of them will be chosen.

1. Memory Based Mechanism
To increase the efficiency of the occ function, a significant amout of statistical information should be stored, either in the memory or on the disk as an index file. For the memory based mechanism, this information is stored in a 2D array called matrix. The second dimension of this array is the size of the ASCII table, i.e. 128. We could have made this dimension smaller to gain a better storage utilization since around 30 symbols in the ASCII table actually have nothing to do with our program. However, by experimentation we found that the time benefit for this alternative is not obvious and the code complexity gets increased. Therefore, the size of the second dimension is still 128. As to the first dimension, if there was no storage limit, its size should have been equal to the size of the input file so that we could get the occ information simply by accessing matrix. Given a memory limit, we can only store the occ information once for every x input symbols. From 12MB we can figure out the maximum size of the first dimension, with which and the size of the input file we can decide the exact value for x. That is, x varies with the size of the input file.
Since the given memory is quite limited, this mechanism is only suitable for small input files. For our implementation, the threshold between small and large files is around 10MB. 

Besides matrix, another array called preceding_char_num, which corresponds to the C array from the lecture slides, is also vital for the implementation. Both matrix and preceding_char_num are generated when reading the input file and we only need to read it once. Another thing to mention is that no index file gets written for this mechanism.

2. Index Based Mechanism
This mechanism is quite similar with the above. The difference is that the information is stored on the disk rather than in the memory and x is fixed. Naturally we want x to be as small as possible, but it can not be too small, otherwise the index file will be larger than the original input file. Since the size of the second dimension of matrix is 128, every row of the occ table should occupy 128 * sizeof(int) = 512bytes. Therefore, x should be a little larger than 512. We make it 600. 

During the first execution for a large input file, every row of the occ table gets written into the index file as a binary form, and so is preceding_char_num in the end. As to the index file organization, the first part of the index file is the occ table and the second part is preceding_char_num. Since the size of preceding_char_num is small, we also keep it in the memory, but it is another story for the occ table. When an occ value is needed, we simply calculate the exact position in the index file, go there by fseek, and read the integer(let's call it a). After that, we read a small portion of bytes (at most 600-1=599 bytes, which is x minus 1) from the input file, and count the occurrence (let's call it b) of the target character. Finally the value we need is a+b.

During the subsequent queries for a large input file, we simply read preceding_char_num from the index file. For the occ information, the action is the same as the above.


Finally, we should mention the '-r' and '-a' modes. Once we get the matching range of the target string, we start from each position within this range and iteratively search the preceding character until we see ']'. Afterwards we extract the offset and that is it.
