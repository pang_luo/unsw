/* 
 * BWT Backward Search
 * 
 * Two modes are involved: the memory mode for small files and the index
 * mode for large files. The threshold for the file size is around 10MB.
 * Please refer to README.txt for more details.
 *
 * Written by Pang Luo
 * Last modified on 25/04/2016
*/

#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/* The input file is read within multiple times. The number of bytes read
   every time is decided by CHUNK_SIZE and search_chunk_size(see below). */
#define CHUNK_SIZE 10000

// The size of the ASCII table
#define ALPHABET_SIZE 128

/* A file of which the size exceeds threshold will be regarded as a large
   file. */
#define threshold 10000000

/* matrix (defined in main) is a 2D array storing occ information. matrix_size
   is the size of the first dimension of this array and MAX_MATRIX_SIZE the
   upper bound. Memory mode only. */
#define MAX_MATRIX_SIZE 17000
int matrix_size;

/* Specify how many bytes of input data corresponds to a row in the occ table.
   Fixed for the index mode, variable for the memory mode. */
int search_chunk_size = 600;

char buffer[CHUNK_SIZE];

// Corresponds to the C array from the lecture slides.
int preceding_char_num[ALPHABET_SIZE];

FILE *inputFileHandle;
FILE *indexFileHandle;

// occ function for the index mode
int occ_index(char c, int pos, int **matrix)
{
    int rounds = pos / search_chunk_size;
    int result = 0;

    // Get the base value from the index file.
    if (rounds > 0) {
        fseek(indexFileHandle, ALPHABET_SIZE * sizeof(int) * (rounds - 1) + sizeof(int) * c, SEEK_SET);
        fread(&result, sizeof(int), 1, indexFileHandle);
    }

    // Get the offset value from the input file.
    fseek(inputFileHandle, rounds * search_chunk_size, SEEK_SET);
    /* By careful calculation and variable assignment we make the size of
       buffer always larger than search_chunk_size so that fread is safe. */
    fread(buffer, 1, pos % search_chunk_size, inputFileHandle); 
    for (int i = 0; i < pos % search_chunk_size; ++i)
        if (buffer[i] == c)
	    ++result;

    return result;
}

// occ function for the memory mode
int occ_memory(char c, int pos, int **matrix)
{
    int rounds = pos / search_chunk_size;
    int result = 0;

    // Get the base value from memory.
    if (rounds > 0)
        result = matrix[rounds - 1][c];

    // Get the offset value from the input file.
    fseek(inputFileHandle, rounds * search_chunk_size, SEEK_SET);
    /* By careful calculation and variable assignment we make the size of
       buffer always larger than search_chunk_size so that fread is safe. */
    fread(buffer, 1, pos % search_chunk_size, inputFileHandle);
    for (int i = 0; i < pos % search_chunk_size; ++i)
        if (buffer[i] == c)
	    ++result;

    return result;
}

int compare(const void *a, const void *b)
{
    return *(const unsigned int *)a - *(const unsigned int *)b;
}

// For -r and -a options
void solve_records(char *option, int (*occ)(char, int, int **), int **matrix, int first, int last)
{
    unsigned int ids[last - first + 1];
    int first_pos;
    char last_char;
    int closed_bracket_seen;
    unsigned int id;
    unsigned int rank;
    for (int i = first; i <= last; ++i) {
        first_pos = i;
	closed_bracket_seen = 0;
	id = 0;
	rank = 1; 
        while (1) {
            fseek(inputFileHandle, first_pos - 1, SEEK_SET);
	    last_char = fgetc(inputFileHandle);
	    if (last_char == '[')
	        break;
	    else if (last_char == ']')
	        closed_bracket_seen = 1;
	    else if (closed_bracket_seen) {
	        id += (last_char - 48) * rank;
		rank *= 10;
	    }
	    first_pos = preceding_char_num[last_char] + occ(last_char, first_pos, matrix);
        }
        ids[i - first] = id;
    }

    qsort(ids, last - first + 1, sizeof(unsigned int), compare);

    if (strcmp(option, "-a") == 0) {
        unsigned int previous_id = ids[0];
	printf("[%u]\n", previous_id);
        for (int i = 1; i <= last - first; ++i)
	    if (ids[i] != previous_id) {
	        previous_id = ids[i];
	        printf("[%u]\n", previous_id);
	    }
    }
    else if (strcmp(option, "-r") == 0) {
        unsigned int previous_id = ids[0];
	int sum = 1;
	for (int i = 1; i <= last - first; ++i)
	    if (ids[i] != previous_id) {
	        previous_id = ids[i];
		++sum;
	    }
	printf("%i\n", sum);
    }
}

void solve(char *option, char *target, int total, int (*occ)(char, int, int **), int **matrix)
{
    int i = strlen(target) - 1;
    char c = target[i];
    int first = preceding_char_num[c] + 1;
    
    /* For every character which is not in the input file, its
       preceding_char_num value is -1 and hence first is 0. */
    if (first == 0) {
        printf("No match.\n");
        return;
    }
    int last;

    while ((c < ALPHABET_SIZE - 1) && (preceding_char_num[c + 1] == -1))
        ++c;
    if (c == ALPHABET_SIZE - 1)
        last = total;
    else
        last = preceding_char_num[c + 1];
    
    while (first <= last && i >= 1) {
        c = target[i - 1];
	first = preceding_char_num[c] + occ(c, first - 1, matrix) + 1;
	last = preceding_char_num[c] + occ(c, last, matrix);
	--i;
    }

    if (last < first) {
        printf("No match.\n");
	return;
    }

    if (strcmp(option, "-n") == 0) {
	printf("%i\n", last - first + 1);
	return;
    }
    
    // For -r and -a options
    solve_records(option, occ, matrix, first, last);
}

/* For the index mode: generating preceding_char_num and writing the occ
   information into the index file. */
void initialize_index()
{
    int read_chunk_size = CHUNK_SIZE / search_chunk_size * search_chunk_size;
    int loops;
    char *p;

    while (! feof(inputFileHandle)) {
        // The size of buffer is always larger than or equal to read_chunk_size. 
        int nitems = fread(buffer, 1, read_chunk_size, inputFileHandle);
        p = buffer;
	while (nitems) {
	    if (nitems >= search_chunk_size)
	        loops = search_chunk_size;
	    else
		loops = nitems;
            for (int i = 0; i < loops; ++i) {
                preceding_char_num[*p] += 1;
	        ++p;
            }
	    /* If nitems is smaller than search_chunk_size, we don't write the
	       corresponding occ information into the index file for the
	       convenience of later calculation. */
	    if (nitems >= search_chunk_size)
	        fwrite(preceding_char_num, sizeof(int), ALPHABET_SIZE, indexFileHandle);
	    nitems -= loops;
	}
    }
}

// For the memory mode: generating preceding_char_num and matrix.
void initialize_memory(int **matrix)
{
    int read_chunk_size = CHUNK_SIZE / search_chunk_size * search_chunk_size;
    int loops;
    char *p;
    int round = 0;
    
    while (! feof(inputFileHandle)) {
	// The size of buffer is always larger than or equal to read_chunk_size.
        int nitems = fread(buffer, 1, read_chunk_size, inputFileHandle);
        p = buffer;
	while (nitems) {
	    if (nitems >= search_chunk_size)
	        loops = search_chunk_size;
	    else
		loops = nitems;
            for (int i = 0; i < loops; ++i) {
                preceding_char_num[*p] += 1;
                matrix[round][*p] += 1;
	        ++p;
            }
	    if (round < matrix_size - 1)
		memcpy(matrix[round + 1], matrix[round], sizeof(int) * ALPHABET_SIZE);
	    ++round;
	    nitems -= loops;
	}
    }
}

int main(int argc, char *argv[])
{
    int **matrix;
    int index_file_exist = 0;

    indexFileHandle = fopen(argv[3], "r");
    inputFileHandle = fopen(argv[2], "r");
    fseek(inputFileHandle, 0, SEEK_END);
    int total = ftell(inputFileHandle);
    fseek(inputFileHandle, 0, SEEK_SET);
  
    /* We never generate an index file if the input file is small. Therefore,
       if there is an index file, the corresponding input file must be 
       large. */
    if (indexFileHandle) {
        index_file_exist = 1;
	fseek(indexFileHandle, ALPHABET_SIZE * sizeof(int) * (total / search_chunk_size), SEEK_SET);
        fread(preceding_char_num, sizeof(int), ALPHABET_SIZE, indexFileHandle);
    }
    else {
        // If the input file is small...
        if (total < threshold) {
	    matrix_size = (total < MAX_MATRIX_SIZE) ? total : MAX_MATRIX_SIZE;
	    matrix = calloc(matrix_size, sizeof(int *));
            for (int i = 0; i < matrix_size; ++i)
                matrix[i] = calloc(ALPHABET_SIZE, sizeof(int));

	    search_chunk_size = ceil((double) total / matrix_size);
            initialize_memory(matrix);
	}
	// If the input file is large...
        else {
            indexFileHandle = fopen(argv[3], "w+");
            initialize_index();
        }
   
        int sum = 0;
	int temp;
	/* Up to this point, preceding_char_num actually stores the occurrence
	   of each character. We need to do a conversion to implement the
	   real C array (C array is the concept from the lecture slides). */
        for (int i = 0; i < ALPHABET_SIZE; ++i) {
            if (preceding_char_num[i]) {
	        temp = preceding_char_num[i];
	        preceding_char_num[i] = sum;
	        sum += temp;
	    }
	    else
	        preceding_char_num[i] = -1;
        }
    }

    if (total < threshold) {
        solve(argv[1], argv[4], total, occ_memory, matrix);
        for (int i = 0; i < matrix_size; ++i)
            free(matrix[i]);
        free(matrix);
    }
    else {
        if (! index_file_exist) {
            fwrite(preceding_char_num, sizeof(int), ALPHABET_SIZE, indexFileHandle);
	    fflush(indexFileHandle);
	}
        solve(argv[1], argv[4], total, occ_index, NULL);
	fclose(indexFileHandle);
    }

    fclose(inputFileHandle);

    return 0;
}
