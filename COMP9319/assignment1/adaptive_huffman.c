/*
 * Function Implementation for Adaptive Huffman Encoder/Decoder
 * Please refer to adaptive_huffman.h for the purpose of every function
 * and global variable
 *
 * Written by Pang Luo
 * Last modified on 28/03/2016
*/

#include <stdio.h>
#include <stdlib.h>
#include "adaptive_huffman.h"

void initialize()
{
    k = 0;
    min_number = 2 * ALPHABET_SIZE - 2;

    root = (Node *) malloc(sizeof(Node));
    root->parent = NULL;
    root->left = NULL;
    root->right = NULL;
    root->weight = 0;
    root->number = min_number;
    NYT = root;

    for (int i = 0; i < 2 * ALPHABET_SIZE - 1; ++i)
        numberToNode[i] = NULL;

    for (int i = 0; i < ALPHABET_SIZE; ++i)
        charToNode[i] = NULL;

    numberToNode[min_number] = root;
}

void print_path(Node *node)
{
    int bits[ALPHABET_SIZE];
    int i = 0;

    // Traverse up the tree
    while (node != root) {
        if ((node->parent)->left == node)
	    bits[i] = 0;
	else
	    bits[i] = 1;
	++i;
	node = node->parent;
    }

    for (int j = i - 1; j >= 0; --j)
        printf("%i", bits[j]);
}

void encode(unsigned char ch)
{
    Node *node = charToNode[ch];
    // If node isn't NULL, it denotes a char which has occurred before 
    if (node) {
        print_path(node);
    }
    // ch is a new char: print the path and the bits for the char
    else {
        print_path(NYT);
        int bits[8] = {0};
        int i = 7;
        while(ch) {
	    bits[i--] = ch % 2;
	    ch /= 2;
	}
	for (i = 0; i < 8; ++i)
	    printf("%i", bits[i]);
    }
}

unsigned char* decode_update(unsigned char *p)
{
    Node *node = root;
    unsigned char ch;
    while (1) {
        if (node == NYT) {
	    ch = 0;
	    for (int i = 0; i < 8; ++i) {
		ch = ch * 2 + (*p - '0');
	        ++p;
	    }
	    break;
	}
	else if (node->left == NULL) {
	    ch = node->ch;
	    break;
	}
	else {
	    if (*p == '0')
	        node = node->left;
	    else
	        node = node->right;
	    ++p;
	}
    }

    printf("%c", ch);
    update(ch);
    
    return p;
}

void slide(Node *p)
{
    // p is the node which should be slid ahead(meaning to the right/top)
    // in the tree
    int wt = p->weight;
    Node *next_node = numberToNode[p->number + 1];

    Node *parent = p->parent;
    int number = p->number;
    int is_left_child = (p == (p->parent)->left);

    Node *temp_parent;
    int temp_number;
    int temp_is_left_child;

    while ((p->left == NULL && next_node->left && next_node->weight == wt) ||
        (p->left && next_node->left == NULL && next_node->weight == wt + 1)) {
	// Keep some properties of next_node, which should be slid backwards
	// (meaning to the left/bottom) in the tree
	temp_parent = next_node->parent;
	temp_number = next_node->number;
        temp_is_left_child = (next_node == (next_node->parent)->left);

        // next_node "inherits" partial status of the node of which the number is
	// one less than next_node's
	next_node->parent = parent;
	if (is_left_child)
	    parent->left = next_node;
	else
	    parent->right = next_node;
	next_node->number = number;
	numberToNode[number] = next_node;

	parent = temp_parent;
	number = temp_number;
	is_left_child = temp_is_left_child;
	next_node = numberToNode[number + 1];
    }
    // Now that p has been slid to its deserved position, we need to give it
    // the new status
    p->parent = parent;
    if (is_left_child)
        parent->left = p;
    else
        parent->right = p;
    p->number = number;
    numberToNode[number] = p;
}

Node* slide_increment(Node *p)
{
    int wt = p->weight;
    Node *next_node = numberToNode[p->number + 1];

    if ((p->left == NULL && next_node->left && next_node->weight == wt) ||
        (p->left && next_node->left == NULL && next_node->weight == wt + 1)) {
	Node *original_parent = p->parent;
        slide(p);
	p->weight = wt + 1;
	if (p->left)
	    p = original_parent;
	else
	    p = p->parent;
    }
    else {
        p->weight = wt + 1;
	p = p->parent;
    }

    return p;
}

void update(unsigned char ch)
{
    Node *leafToIncrement = NULL;
    Node *q = charToNode[ch];
    // Special case 1: if ch is a new char and there's one more char which
    // hasn't occurred in the alphabet besides ch, then split the NYT node
    if (q == NULL && k < ALPHABET_SIZE - 1) {
        ++k;
	Node *left = (Node *) malloc(sizeof(Node));
	left->parent = NYT;
	left->left = NULL;
	left->right = NULL;
	left->weight = 0;
	left->number = min_number - 2;
        numberToNode[min_number - 2] = left;

        Node *right = (Node *) malloc(sizeof(Node));
	right->parent = NYT;
	right->left = NULL;
	right->right = NULL;
	right->weight = 0;
	right->number = min_number - 1;
	right->ch = ch;
        numberToNode[min_number - 1] = right;

	min_number -= 2;
        NYT->left = left;
	NYT->right = right;
        q = NYT;
	NYT = left;
	leafToIncrement = right;
	charToNode[ch] = right;
    }
    else {
        // If ch is a new char and is the only char that hasn't occurred
	// in the alphabet, let the NYT node denote ch rather than split
	// it. 
	// Note: this condition never happens for this assignment because
	// the size of the alphabet is 256, but '\0' or '\n' never shows
	// up as a char to encode.
	// Just for logic completeness, I implement it here.
	if (q == NULL && k == ALPHABET_SIZE - 1) {
	    ++k;
	    q = NYT;
	    q->ch = ch;
	    charToNode[ch] = q;
	    NYT = NULL;
	}

        // Find the leader of q's block
        int leader_number = q->number + 1;
	while (numberToNode[leader_number]->weight == q->weight &&
	    numberToNode[leader_number]->left == NULL)
	    ++leader_number;
        --leader_number;

        // Let q and the leader of its block exchange necessary status
	if (q->number != leader_number) {
	    Node *leader_node = numberToNode[leader_number];
	    unsigned char leader_ch = leader_node->ch;
	    q->ch = leader_ch;
	    leader_node->ch = ch;
	    charToNode[ch] = leader_node;
	    charToNode[leader_ch] = q;
	    q = leader_node;
	}

        // Special case 2: q is the sibling of the NYT node
	if (NYT && q->parent == NYT->parent) {
	    leafToIncrement = q;
	    q = q->parent;
	}
    }
    
    while (q != root)
        q = slide_increment(q);
    root->weight += 1;

    // Handle the two special cases
    if (leafToIncrement)
        slide_increment(leafToIncrement);
}

void free_resource(Node *node)
{
    if (node) {
        free_resource(node->left);
	free_resource(node->right);
        free(node);
    }
}

