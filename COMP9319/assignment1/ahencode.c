/*
 * Adaptive Huffman Encoder
 * 
 * Written by Pang Luo
 * Last modified on 28/03/2016
*/

#include <stdio.h>
#include <string.h>
#include "adaptive_huffman.h"

int main(int argc, char *argv[])
{
    // By allocating an array with size MAX_CHAR_NUM + 2, we make sure it is
    // sufficient for storing MAX_CHAR_NUM characters, a '\n' and a '\0'.
    unsigned char str[MAX_CHAR_NUM + 2];
    int formatted = ((argc == 2) && ! strcmp(argv[1], "-s"));

    while (fgets((char *) str, MAX_CHAR_NUM + 2, stdin)) {
        initialize();
        for (unsigned char *p = str; *p != '\n'; ++p) {
	    if (formatted && p != str)
	        printf(" ");
	    encode(*p);
	    update(*p);
	}
	free_resource(root);
	printf("\n");
    }

    return 0;
}
