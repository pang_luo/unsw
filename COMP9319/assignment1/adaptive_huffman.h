/*
 * Header for Adaptive Huffman Encoder/Decoder
 * 
 * Written by Pang Luo
 * Last modified on 28/03/2016
*/

#ifndef ADAPTIVE_HUFFMAN_H
#define ADAPTIVE_HUFFMAN_H

#define MAX_CHAR_NUM 1000
#define MAX_DIGIT_NUM 8000
#define ALPHABET_SIZE 256

typedef struct Node
{
    struct Node *parent;
    struct Node *left;
    struct Node *right;
    int weight;
    int number;
    unsigned char ch;
}    Node;

// the number of distinct characters that are processed so far
int k;

// the minimum node number
int min_number;

Node *root;
Node *NYT;

// numberToNode[i] is the corresponding Node pointer for an implicit number i
Node *numberToNode[2 * ALPHABET_SIZE - 1];

// charToNode[ch] is the corresponding Node pointer for a character ch
Node *charToNode[ALPHABET_SIZE];

// Tree structure and variables initialization
void initialize();

// Given a node pointer, print the path from the root to the node.
void print_path(Node *node);

// Print the encoded bits for a char
void encode(unsigned char ch);

// Print the decoded char, update the tree, and return a pointer referring to
// the next digit waiting to be processed
unsigned char* decode_update(unsigned char *p);

// Slide a node ahead in the tree
void slide(Node *p);

// Node slide and weight increment
// Return p's original parent if p is internal, otherwise
// return p's new parent
Node* slide_increment(Node *p);

// Update the tree structure
void update(unsigned char ch);

// Release allocated resources
void free_resource(Node *node);

#endif
