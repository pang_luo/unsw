* Algorithm and Implementation Mechanism for Assignment 1
* Written by Pang Luo
* Last modified on 28/03/2016
* This program assumes that every line for the encode/decoder has at most 1000 characters/8000 digits. To accommodate more characters/digits per line, just modify the macros MAX_CHAR_NUM and MAX_DIGIT_NUM in adaptive_huffman.h.

We'll refer to some variables and functions below. Hopefully their names imply their purpose. For any vagueness, please refer to adaptive_huffman.h, which has detailed comments.

1. Overall Mechanism
The overall mechanism can be described by the following Python-style pseudocode.

1.1 For Encoder:
for every line:
    initialize the tree structure and status variables
    for every char in the line:
        encode the char
	update the tree
    free resources

1.2 For Decoder:
for every line:
    initialize the tree structure and status variables
    while True:
        repeat:
            read a digit, traverse down the tree 
        until getting to a leaf node

        if the leaf node is the NYT node:
            read next eight digits and print the corresponding char
        else:
            print the char denoted by the leaf node
        
	update the tree
	if the char waiting to be processed is '\n':
	    break
    free resources


2. Data Structure
We use a binary tree as the main data structure. A node is a struct consisting of six members: parent, left, right, weight, number and ch. The meanings of the first four are clear. number denotes the implicit number of the node, while ch represents the character denoted by this node. Now we can easily access the corresponding number and character once given a node pointer. Also, we introduce two arrays numberToNode and charToNode so that we can access the node given a number or a character. These two arrays (or maps) turn out to be vital for the program efficiency. In the function update, given a node pointer q we have to search the leader of its block. With numberToNode this becomes quite easy because we only need to check numberToNode[q->number + 1], numberToNode[q->number + 2] and so on. This array also plays an important role in the sliding process. As to charToNode, when a character ch is encountered in the encoding process, we can access its node easily by charToNode[ch]. It should be noted that in order to guarantee efficiency each element of charToNode is initialized to be NULL, rather than the NYT node.
We also introduce two variables k and min_number. k denotes the number of distinct characters that are processed so far, and min_number the minimum node number.


3. Algorithm
The most important functions in this program are: update, slide_increment and slide. Below we'll explain the algorithm involved.

3.1 update
Given a character ch, this function will update the tree structure. The process is as follows.
By charToNode we get the node q for ch. If ch is a new char(the actual condition is more complicated than this, please refer to adaptive_huffman.c), split the NYT node to produce two children(the left child is the new NYT node, while the right denotes ch and would be marked as "leafToIncrement"), otherwise we do the following: locate the leader of q's block, exchange the characters stored in q and the leader, and update the charToNode array; if q is the sibling of the NYT node, we mark it as "leafToIncrement". 
Next we alternately call slide_increment(q) and move q up the tree until q becomes the root. At last we increase root's weight by one, and call slide_increment for "leafToIncrement".

3.2 slide_increment
The idea is pretty simple. For node q of which the weight is to be increased, we check whether Vitter's invariant will be maintained after the increment. If not, we slide q ahead in the tree to the proper position, and slide the other nodes involved backwards. After sliding q's weight will be increased by 1. There are two problems lying here. First, how can we access the nodes ahead of q? This is solved by accessing numberToNode[q->number + i] where i = 1, 2 and so on. Second, how can we implement the function slide efficiently?

3.3 slide
The algorithm of slide is best explained by an example. Assume we have four nodes: a1, a2, a3 and a4. The implicit numbers of them are increasing. Now we want to slide a1 ahead of a4. To this end, first we slide a2 to a1's position, which means a1's parent and number will become a2's. In the same way we slide a3 to a2's original position, which means a2's original parent and original number will become a3's. The same thing happens to a4. Now we can slide a1 to a4's original position, which means a4's original parent and original number will become a1's. That's it. The best thing of this algorithm is that during the whole process, even if any of the four nodes is internal, we don't need to change the status of its descendants.

It turns out that this program is efficient. On a typical CSE linux machine, it runs the encoding-decoding cycle instantly for a file containing 80,000 characters.
