/*
 * Adaptive Huffman Decoder
 * 
 * Written by Pang Luo
 * Last modified on 28/03/2016
*/

#include <stdio.h>
#include <string.h>
#include "adaptive_huffman.h"

int main(int argc, char *argv[])
{
    // By allocating an array with proper size, we make sure it is
    // sufficient for storing given digits(and spaces in -s mode),
    // a '\n' and a '\0'.
    unsigned char str[2 * MAX_DIGIT_NUM + 2];
    unsigned char *p;
    int formatted = ((argc == 2) && ! strcmp(argv[1], "-s"));

    while (fgets((char *) str, 2 * MAX_DIGIT_NUM + 2, stdin)) {
        // When meeting with an empty line, we simply ignore it
	// and print '\n'.
        if (str[0] == '\n') {
	    printf("\n");
	    continue;
	}
        initialize();
	p = str;
	while(1) {
	    p = decode_update(p);
	    if (formatted && *p == ' ')
	        ++p;
	    if (*p == '\n')
	        break;
	}
	free_resource(root);
	printf("\n");
    }

    return 0;
}
