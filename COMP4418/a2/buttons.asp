#show press_a/1.
#show press_b/1.

time(0..2).

% Initial conditions
% Since the initial conditions are 'not p(0)' and 'not q(0)', which are both
%   negative, there is no need to state them explicitly.

% Generator rule
1 { press_a(T); press_b(T) } 1 :- time(T).

% Preconditions
% When p and q are both on, press_b will exchange the status of p and q, 
%   meaning p and q will be still on. Therefore, we don't need to exclude
%   this condition from the preconditions of press_b. The case is similar 
%    when p and q are both off.

% Effects
p(T+1) :- q(T), press_b(T), time(T).
p(T+1) :- not p(T), press_a(T), time(T).

q(T+1) :- q(T), not press_b(T), time(T).
q(T+1) :- p(T), press_b(T), time(T).


% Goal
:- not p(3).
:- not q(3).

