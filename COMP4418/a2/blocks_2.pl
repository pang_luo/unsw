:- ['gologinterpreter.swi'].

:- discontiguous(block/2).
:- discontiguous(on/3).
:- discontiguous(clear/2).

/* auxiliary definition */

restoreSitArg(block(X),S,block(X,S)).
restoreSitArg(on(X,Y),S,on(X,Y,S)).
restoreSitArg(clear(X),S,clear(X,S)).
restoreSitArg(goal,S,goal(S)).

primitive_action(moveOffTable(_,_)).
primitive_action(moveToTable(_)).

/* sample goal predicate */
/*
goal(S) :- on(a,b,S), on(b,c,S), on(c,d,S), on(d,e,S).
*/


goal(S) :- on(e,d,S), on(d,c,S), on(c,b,S), on(b,a,S), clear(e,S).

/*
goal(S) :- on(d,c,S), on(c,a,S), clear(b,S), clear(d,S), clear(e,S).
*/
/* sample initial state */

block(a,s0).
block(b,s0).
block(c,s0).
block(d,s0).
block(e,s0).
/*
on(d,c,s0).
on(c,a,s0).
clear(b,s0).
clear(d,s0).
clear(e,s0).
*/


on(a,b,s0).
on(b,c,s0).
on(c,d,s0).
on(d,e,s0).
clear(a,s0).


/* Precondition Axioms */

poss(moveToTable(X), S) :-
    block(X, S),
    clear(X, S).

poss(moveOffTable(X, Y), S) :-
    block(X, S),
    block(Y, S),
    not X = Y,
    clear(X, S),
    clear(Y, S),
    not (block(Z, S), on(X, Z, S)).

/* Successor State Axioms */

block(X, do(_, S)) :- block(X, S).

on(X, Y, do(A, _)) :- A = moveOffTable(X, Y).

on(X, Y, do(A, S)) :-
    on(X, Y, S),
    not A = moveToTable(X).

clear(X, do(A, S)) :-
    on(Z, X, S),
    A = moveToTable(Z).

clear(X, do(A, S)) :-
    clear(X, S),
    not A = moveOffTable(_, X).

/* GOLOG Procedures */

proc(flatten, 
    while(some(x, some(y, block(x) & block(y) & on(x, y) & clear(x))),
        pi(x, pi(y, ?(block(y)) : ?(on(x, y)) : moveToTable(x)))
    )
).

proc(solve,
    if(-goal,
        flatten :
        while(-goal,
            pi(x, pi(y, moveOffTable(x, y)))))).

