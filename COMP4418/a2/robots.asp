#show does/3.

% The length of the shortest solution is 17.
time(0..17).

1 { does(robot,M,T) : move_domain(M) } 1 :- time(T), not terminal(T).
:- does(robot,M,T), not legal(robot,M,T).
:- 0 { terminal(T) : time(T) } 0.
:- terminal(T), not terminal(T-1), not goal(robot,100,T).

move_domain(move(X1,Y1,X2,Y2)) :- coord_x(X1), coord_x(X2), coord_y(Y1), coord_y(Y2).
coord_x(1).
coord_x(2).
coord_x(3).
coord_y(1).
coord_y(2).

% The rescue robot initially at (1,1) is called 2, 
%   the one initially at (1, 2) is called 3, 
%   all the other robots are called 1.
holds(cell(1,1,2),0).
holds(cell(1,2,3),0).
holds(cell(2,1,0),0).
holds(cell(2,2,1),0).
holds(cell(3,1,1),0).
holds(cell(3,2,1),0).

legal(robot,move(X1, Y1, X2, Y2),T) :-
    occupied(X1,Y1,T),
    holds(cell(X2,Y2,0),T),
    |X1 - X2| + |Y1 - Y2| == 1.

holds(cell(X1,Y1,0),T+1) :-
    does(robot,move(X1,Y1,X2,Y2),T).

holds(cell(X2,Y2,C),T+1) :-
    holds(cell(X1,Y1,C),T),
    does(robot,move(X1,Y1,X2,Y2),T).

holds(cell(X,Y,C),T+1) :-
    holds(cell(X,Y,C),T),
    does(robot,move(X1,Y1,X2,Y2),T),
    X != X1,
    X != X2.

holds(cell(X,Y,C),T+1) :-
    holds(cell(X,Y,C),T),
    does(robot,move(X1,Y1,X2,Y2),T),
    X != X1,
    Y != Y2.

holds(cell(X,Y,C),T+1) :-
    holds(cell(X,Y,C),T),
    does(robot,move(X1,Y1,X2,Y2),T),
    Y != Y1,
    X != X2.

holds(cell(X,Y,C),T+1) :-
    holds(cell(X,Y,C),T),
    does(robot,move(X1,Y1,X2,Y2),T),
    Y != Y1,
    Y != Y2.

occupied(X,Y,T) :-
    holds(cell(X,Y,1),T).

occupied(X,Y,T) :-
    holds(cell(X,Y,2),T).

occupied(X,Y,T) :-
    holds(cell(X,Y,3),T).

goal(robot,100,T) :-
    holds(cell(1,1,3),T),
    holds(cell(1,2,2),T).

terminal(T) :-
    goal(robot,100,T),
    time(T).
