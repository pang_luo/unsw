# A backchaining implementation
#
# Written by Pang Luo
# Last modified on 27/08/2015


def solve(goalList, activeList, goalStates, kb):
    """Judges whether or not the given goal list is entailed.

    Args:
        goalList (list): an list of goals.
        activeList (list): the currently active rules.
        goalStates (dict): a dictionary with a goal as a key and a state
          as a value. A state of 'T' means the goal is entailed,
          while an 'F' means not.
        kb (list): a list of positive Horn clauses.

    Returns:
        bool: True if goalList is entailed, false otherwise.

    Others:
        '-' is used as the negation symbol.
    """
    if len(goalList) == 0:
        return True

    if goalStates.get(goalList[0]) == 'T':
        return solve(goalList[1:], activeList, goalStates, kb)

    if goalStates.get(goalList[0]) == 'F':
        return False

    for clause in kb:
        if goalList[0] in clause and clause not in activeList:
            assumptionList = list(clause) # Now assumptionList has a form
                                          #   like ['-a', '-b', 'c']    
            assumptionList.remove(goalList[0]) # Now assumptionList has
                                               #   a form like ['-a', '-b']
            for i in range(len(assumptionList)):
                if assumptionList[i].startswith('-'):
                    assumptionList[i] = assumptionList[i][1:]
                else:
                    assumptionList[i] = '%s%s' % ('-', assumptionList[i])
            # After the above processing, now assumptionList has a form like
            #   ['a', 'b']. In other words, 'c' has been removed, and '-'
            #   before 'a' and 'b' is eliminated.
            if solve(assumptionList, activeList + [clause], goalStates, kb):
                goalStates[goalList[0]] = 'T'
                return solve(goalList, activeList, goalStates, kb)

    goalStates[goalList[0]] = 'F'
    return False


def is_satisfiable(clauseList):
    """Judges whether or not the given clauses are satisfiable by means of
       calling the function 'solve'.

    Args:
        clauseList (list): a list representing a collection of
          propositional Horn clauses, positive and negative.

    Returns:
        bool: True if the given clauses are satisfiable, false otherwise.

    Others:
        '-' is used as the negation symbol.
    """
    kb = []
    goalsList = []  # goalsList is different from goalList in the solve
                    #   function. Every element of a goalsList is a list of
                    #   goals, while every element of a goalList is a goal.
    for clause in clauseList:
        nPositive = sum(1 for i in clause if not i.startswith('-'))
        # If the number of positive literals in a clause is larger than 1,
        #   raise an exception since the clause is not an Horn clause.
        if nPositive > 1:
            raise ValueError("The argument must be a Horn clause list.")
        elif nPositive == 1:
            kb.append(clause)    # Add the clause to the kb if it's positive.
        else:
            # Get a list of goals according to the clause
            #   by eliminating the negation symbol of every literal
            goals = [literal[1:] for literal in clause]
            goalsList.append(goals) # Add goals to the overall goalsList

    for goals in goalsList:
        if solve(goals, [], {}, kb):
            return False    # If one element of the goalsList is entailed by
                            #   the kb, i.e. the corresponding negative
                            #   clause is not satisfied, return False
                            
    return True  # If none of goals is entailed by the kb, return True

    
if __name__ == '__main__':
    print('Test Case 1')
    clause1 = set(['-b1', 'a1'])
    clause2 = set(['b1', '-a1'])
    clause3 = set(['-b4', 'a1'])
    clause4 = set(['b4'])
    kb = [clause1, clause2, clause3, clause4]
    print('kb:', kb)
    
    goal1 = ['a1']
    goal2 = ['b1']
    goal3 = ['a1', 'b1']
    goal4 = ['-a1', 'b1']
    for goal in (goal1, goal2, goal3, goal4):
        if solve(goal, [], {}, kb):
            print('%s is entailed.' % goal)
        else:
            print('%s is not entailed.' % goal)
    print()
    
    clauseList = list(kb)
    clauseList.append(set(['-a1', '-d1']))
    clauseList.append(set(['-b1', '-d1']))
    print('Horn clause collection:', clauseList)
    if is_satisfiable(clauseList):
        print('Satisfiable')
    else:
        print('Unsatisfiable')
    print()
    
    clauseList.append(set(['-a1', '-b1']))
    print('Horn clause collection:', clauseList)
    if is_satisfiable(clauseList):
        print('Satisfiable')
    else:
        print('Unsatisfiable')
    print()
    print()

    print('Test Case 2')
    clause5 = set(['Toddler'])
    clause6 = set(['-Toddler', 'Child'])
    clause7 = set(['-Child', '-Male', 'Boy'])
    clause8 = set(['-Infant', 'Child'])
    clause9 = set(['-Child', '-Female', 'Girl'])
    clause10 = set(['Female'])
    kb = [clause5, clause6, clause7, clause8, clause9, clause10]
    print('kb:', kb)

    goal1 = ['Girl', 'Female']
    goal2 = ['Girl', 'Male']
    goal3 = ['Boy']
    goal4 = ['-Girl']
    for goal in (goal1, goal2, goal3, goal4):
        if solve(goal, [], {}, kb):
            print('%s is entailed.' % goal)
        else:
            print('%s is not entailed.' % goal)
    print()

    clauseList = list(kb)
    clauseList.append(set(['-Girl', '-Male']))
    print('Horn clause collection:', clauseList)
    if is_satisfiable(clauseList):
        print('Satisfiable')
    else:
        print('Unsatisfiable')
    print()

    clauseList.append(set(['-Girl', '-Female']))
    print('Horn clause collection:', clauseList)
    if is_satisfiable(clauseList):
        print('Satisfiable')
    else:
        print('Unsatisfiable')



    
