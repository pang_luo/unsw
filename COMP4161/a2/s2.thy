theory s2
imports Main
begin


(**********************************************)
(* PART 1: "Compiling Arithmetic Expressions" *)
(**********************************************)


(* Syntax of the language *)
datatype expr = BinOp "(nat \<Rightarrow> nat \<Rightarrow> nat)" expr expr |
                UnOp "(nat \<Rightarrow> nat)" expr |
                Const nat

definition
  plus :: "nat \<Rightarrow> nat \<Rightarrow> nat"
  where
  "plus \<equiv> (\<lambda>a b. a + b)"
  
lemma "plus = (op +)"
  by(simp add: plus_def)
    
(* Evaluation function *)
primrec
  eval :: "expr \<Rightarrow> nat"
where
  "eval (Const n) = n" |
  "eval (BinOp f a b) = f (eval a) (eval b)" |
  "eval (UnOp f a) = f (eval a)"

lemma "eval (BinOp (op +) (Const 2) (Const 3)) = 5"
  by simp
  
(* Programs for the stack machine *)
datatype stackp = Push nat |
                  DoUnOp "nat \<Rightarrow> nat" |
                  DoBinOp "nat \<Rightarrow> nat \<Rightarrow> nat" |
                  Seq stackp stackp ("_ ;; _")  |
                  Skip

type_synonym stack = "nat list"

(* Big step operational semantics to programs for the stack machine *)
inductive
  sem :: "stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> bool" ("\<langle>_, _\<rangle> \<Down> _")
where
  sem_Push: "sem s (Push n) (n#s)" |
  sem_DoBinOp: "sem (a#b#s) (DoBinOp f) ((f a b)#s)" |
  sem_DoUnOp: "sem (a#s) (DoUnOp f) ((f a)#s)" |
  sem_Seq: "\<lbrakk>sem s a t; sem t b u\<rbrakk> \<Longrightarrow> sem s (Seq a b) u"

(* Simple compiler from expression to stack programs *)
primrec
  compile  :: "expr \<Rightarrow> stackp" 
where
  "compile (Const n) = Push n" |
  "compile (BinOp f a b) = Seq (compile b) (Seq (compile a) (DoBinOp f))" |
  "compile (UnOp f a) = Seq (compile a) (DoUnOp f)"

(* ---------- *)
(* Question 1 *)
(* ---------- *)

(* (a) Prove that the stack machine semantics is deterministic *)
lemma seqE:
  "\<langle>s, a ;; b\<rangle> \<Down> u \<Longrightarrow> (\<And>t. \<langle>s, a\<rangle> \<Down> t \<Longrightarrow> \<langle>t, b\<rangle> \<Down> u \<Longrightarrow> P) \<Longrightarrow> P"
  by (erule sem.cases, auto)
     
lemma sem_det:
  "sem s e t \<Longrightarrow> sem s e u \<Longrightarrow> u = t"  
  apply (induct s e t arbitrary: u rule: sem.induct) thm sem.cases
     apply (erule sem.cases, clarsimp+)
    apply (erule sem.cases, clarsimp+) 
   apply (erule sem.cases, clarsimp+)[1]
  apply (erule seqE, clarsimp)
  done
  
(* Some explanations:

   sem is defined as an inductive predicate, so we should
   use the rule sem.induct. This rule is an elimination rule;
   it will eliminate the first premise unifying with "sem _ _ _"
   (here "sem s e t"). The rule can be used with 
      "erule sem.induct" or "induct s e t rule:sem.induct"
   Note that this would work fine until the Seq case where
   you need to have an arbitrary "u" in the induction hypothesis.
   Then using "induct" makes it easy to say which  variables 
   should be arbitrary.
   
   Then the subgoals have a premise of the form "\<langle>_, ***\<rangle> \<Down> _
   where *** is one of the constructors for programs.
   To eliminate these premises, we may use sem.cases
   which eliminates any "\<langle>_,_\<rangle> \<Down> _" premise, or write
   more custom elimination rules for each program construct,
   which is useful here for the sequence.
*)
  

(* (b) Prove that the compiler is correct *)
lemma compile_correct:
  "sem s (compile e) ((eval e)#s)"
  apply (induct e arbitrary: s)
    apply clarsimp    
    apply (rule sem_Seq, assumption)
    apply (rule sem_Seq, assumption)
    apply (rule sem_DoBinOp)
   apply simp
   apply (rule sem_Seq, assumption)
   apply (rule sem_DoUnOp)
  apply simp
  apply (rule sem_Push)
  done
 
(* Shorter proof:
  by (induct e arbitrary: s, auto intro: sem.intros)
*)
  

(* Some explanations:

  Both compile and eval are defined as primrec on their parameter,
  which is "e" in both cases in this lemma. So we use simple induction on
  the datatype "e". We need "s" to be arbitrary to apply the 
  induction hypothesis. Then we use the introduction rules of the 
  inductively defined predicate "sem".

*)

  
  
  
(* (c) Prove that whether an expression can evaluate 
       depends only on the size of the initial stack *)
       
lemma length_SucD:
  "length t = Suc n \<Longrightarrow> \<exists>a t'. t = a#t' \<and> length t' = n"
  by(case_tac t, auto)
  
lemma sem_stack_content_agnostic_pres:
  "sem s p t \<Longrightarrow> 
  \<forall>s'. length s' = length s \<longrightarrow> 
       (\<exists>t'. sem s' p t' \<and> length t' = length t)"
  apply (induct s p t rule:sem.induct)
     apply clarsimp
     apply (rule exI, rule conjI, rule sem_Push, simp)
    apply clarsimp
    apply (drule length_SucD,clarsimp)+
    apply (rule exI, rule conjI, rule sem_DoBinOp, simp)
   apply clarsimp
   apply (drule length_SucD,clarsimp)
   apply (rule exI, rule conjI, rule sem_DoUnOp, simp)
  apply clarsimp 
  apply (erule allE, erule impE, assumption, clarsimp)+
  apply (rule exI, rule conjI, rule sem_Seq, assumption+)
  done
  
(* Shorter proof:
  apply(induct s p t rule: sem.induct)
     apply(fastforce intro: sem.intros dest: length_SucD)+
  done
*)  
  
(* Some explanations

  Here we again have the main premise being "sem _ _ _"
  so we use the sem.induct rule. There's no variable
  to mark as arbitrary. If we don't have the strengthening
  "length t' = length t" then we can prove the first 3 cases,
  but when reaching the Seq case, the induction hypothesis only
  apply if we know the length of the intermediate stack.
  This is why the strengthening is needed. Then the lemma
  can be proved easily (below).

*)


lemma sem_stack_content_agnostic:
  "sem s p t \<Longrightarrow> \<forall>s'. length s' = length s \<longrightarrow> (\<exists>t'. sem s' p t')"
  by (drule sem_stack_content_agnostic_pres, auto)
  
  

(* ---------- *)
(* Question 2 *)
(* ---------- *)

(* Sufficient initial stack size *)
definition
  reqd_init_stack :: "stackp \<Rightarrow> nat \<Rightarrow> bool"
where
  "reqd_init_stack p h \<equiv> (\<exists>s t. \<langle>s,p\<rangle> \<Down> t) \<longrightarrow> (\<forall>s. length s \<ge> h \<longrightarrow> (\<exists>t. \<langle>s,p\<rangle> \<Down> t))"

(* (a) Prove that compiled expressions require no initial stack *)
lemma compile_reqd_init_stack:
  "reqd_init_stack (compile e) 0"
  apply (clarsimp simp: reqd_init_stack_def)
  apply (rule exI, rule compile_correct)
  done

(* (b) Minimal initial stack length for atomic programs *)
lemma reqd_init_stack_Push:
  "reqd_init_stack (Push n) 0"
  apply (clarsimp simp: reqd_init_stack_def) 
  apply (rule exI, rule sem_Push)
  done

lemma reqd_init_stack_DoUnOp:
  "reqd_init_stack (DoUnOp f) (Suc 0)"
  apply (clarsimp simp: reqd_init_stack_def) 
  apply (rename_tac s')
  apply (case_tac s', simp_all)
  apply (rule exI, rule sem_DoUnOp)
  done

lemma reqd_init_stack_DoBinOp:
  "reqd_init_stack (DoBinOp f) (Suc (Suc 0 ))"
  apply (clarsimp simp: reqd_init_stack_def) 
  apply (rename_tac s')
  apply (case_tac s', simp_all)
  apply (rename_tac x xs)
  apply (case_tac xs, simp_all)
  apply (rule exI, rule sem_DoBinOp)
  done
  
lemma reqd_init_stack_Skip:
  "reqd_init_stack Skip 0"
  apply (clarsimp simp: reqd_init_stack_def) 
  apply (erule sem.cases, simp_all)
  done
  
(* (c) Minimal initial stack length for Seq *)

lemma sem_app:
  "sem s e t \<Longrightarrow> sem (s@u) e (t@u)"
  apply(induct s e t  arbitrary: u rule: sem.induct)
  apply(auto intro: sem.intros)
  done
  
lemma reqd_init_stack_Seq:
  "\<lbrakk>(reqd_init_stack p1) n; (reqd_init_stack p2 m)\<rbrakk>
       \<Longrightarrow> (reqd_init_stack (Seq p1 p2) (n+m))"
  apply (clarsimp simp: reqd_init_stack_def)
  apply (erule seqE) 
  apply (erule impE, blast)+ 
  apply(rename_tac s t s' u)
  
  apply(drule_tac x="take n s'" in spec, clarsimp)
  apply(rename_tac u')
  apply(drule_tac u="drop n s'" in sem_app, simp) 
  apply(drule_tac x="(u' @ drop n s')" in spec)
  apply(erule impE) 
   apply auto[1] 
  apply clarsimp
  apply (rule exI, rule sem_Seq, assumption+)
  done

(* Explanations:

  After unfolding definitions, simplifying a bit, and renaming,
  we need to find a t' such that \<langle>s', p1 ;; p2\<rangle> \<Down> t'
  
  Given the semantics for sequence, this means 
  we need to find a u' and t' such that 
     \<langle>s', p1\<rangle> \<Down> u'
  and
     \<langle>u', p2\<rangle> \<Down> t'
  
  We know (from first assumption of our lemma) that 
  we can find such a u' for any stack s' larger than n.
  
  We also know (from second assumption of our lemma) that
  we can find such a t' for any stack u' larger than m.
  
  s' is larger than n, so we could just use s', but then
  we don't know anything about the size of u'.
  
  Instead we will use the fact that s' = take n s' @ drop n s'
  and the fact that if \<langle>s, e\<rangle> \<Down> t then we can append additional
  stack u on both stacks s and t: "\<langle>s @ u, e\<rangle> \<Down> t @ u"
  
  Then we use the first assumption with "take n s'" (which is
  larger than n), we find a u', and then append to it
  "drop n s'", which we know now is larger than m, so we
  can apply it the second assumption.
  
*)
  
  
(* (d) Define a function that given a program p 
       calculates an appropriate stack length h
       such that reqd_init_stack p h holds *)

primrec
  sufficient_init_stack :: "stackp \<Rightarrow> nat"
where
  "sufficient_init_stack (Push n) = 0" |
  "sufficient_init_stack (DoBinOp f) = (Suc (Suc 0))" |
  "sufficient_init_stack (DoUnOp f) = (Suc 0)" |
  "sufficient_init_stack Skip = 0" |
  "sufficient_init_stack (Seq a b) = (sufficient_init_stack a) + (sufficient_init_stack b)"


(* (e) Prove your function from (d) correct *)
lemma sufficient_init_stack_is_reqd_init_stack:
  "reqd_init_stack p (sufficient_init_stack p)"
  apply (induct p)
  apply (simp_all add: reqd_init_stack_Seq reqd_init_stack_Skip
                       reqd_init_stack_DoBinOp reqd_init_stack_DoUnOp
                       reqd_init_stack_Push) 
  done


  
(* ---------- *)
(* Question 3 *)
(* ---------- *)

(*  Small-step semantics *)
inductive
  sems :: "stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> bool"
where
  "sems s (Push n) (n#s) Skip" |
  "sems (a#b#s) (DoBinOp f) ((f a b)#s) Skip" |
  "sems (a#s) (DoUnOp f) ((f a)#s) Skip" |
  "sems s a s' a' \<Longrightarrow> sems s (Seq a b) s' (Seq a' b)" |
  "sems s (Seq Skip b) s b"


  
(* (a) Define a function semsn:: nat \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> bool
       that executes n steps of the small-step semantics *)

   
primrec
  semsn :: "nat \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> bool"
where
  "semsn 0 s a s' a' = (s = s' \<and> a = a')" |
  "semsn (Suc n) s a s' a' = (\<exists>s'' a''. sems s a s'' a'' \<and> semsn n s'' a'' s' a')"

lemma semsn_simp [iff]:
  "semsn (Suc 0) s a s' a' = sems s a s' a'"
  by simp

(* (b) Prove that if a program a executes in the big-step semantics to a 
       resulting stack t from an initial stack s, then it executes in the
       small-step semantics to the same resulting stack and the resulting
       program Skip. *)  

lemma semsn_add:
  "semsn n s a t b \<Longrightarrow> semsn m t b s' a' \<Longrightarrow> semsn (n+m) s a s' a'"
  apply(induct n arbitrary: m s a t b s' a')
   apply simp
  apply(subst add_Suc)
  apply clarsimp
  apply force
  done

lemma semsn_Seq_unchanged:
  "semsn n s a s' a' \<Longrightarrow> semsn n s (Seq a b) s' (Seq a' b)"
  apply(induct n arbitrary: s a s' a' b)
   apply simp
  apply(fastforce intro: sems.intros)
  done
  
lemma semsn_correct:
  "sem s a t \<Longrightarrow> \<exists>n. semsn n s a t Skip"
  apply(induct s a t rule: sem.induct)
     apply(force intro: sems.intros)+
  apply clarsimp
  apply(rename_tac s a t b u n m)
  apply(rule_tac x="n+(Suc m)" in exI)
  apply(rule semsn_add)
   apply(erule semsn_Seq_unchanged)
  apply simp
  apply(blast intro: sems.intros)
  done


(* (c) Prove that there is no universal stack bound for any compiled program *)
(* Predicate stating that stack size h is a stack bound for program p *)
definition
  stack_bound :: "stackp \<Rightarrow> nat \<Rightarrow> bool"
where
  "stack_bound p h \<equiv> \<forall>s n s' p'. semsn n s p s' p' \<longrightarrow>  length s' - length s \<le> h"
  
primrec
  prog_using_Suc :: "nat \<Rightarrow> expr"
where
  "prog_using_Suc 0 = Const 0" |
  "prog_using_Suc (Suc n) = (BinOp (op +) (prog_using_Suc n) (Const 0))"
  

lemma prog_using_Suc_uses_Suc:
  "\<exists>n s' p'. semsn n s (compile (prog_using_Suc h)) s' p' \<and> length s' - length s > h"
  apply(induct h arbitrary: s)
   apply simp
   apply(rule_tac x="Suc 0" in exI)
   apply(rule_tac x="0#s" in exI)
   apply(fastforce intro: sems.intros)
  apply simp
  apply(drule_tac x="0#s" in meta_spec)
  apply(elim exE)
  apply(rule_tac x="(Suc (Suc n))" in exI)
  apply simp
  apply(rule_tac x="s'" in exI)
  apply(rule conjI)
   defer
   apply fastforce
  apply clarsimp
  apply(rule_tac x="Seq p' (DoBinOp (op +))" in exI)
  apply(rule_tac x="0#s" in exI)
  apply(rule_tac x="Seq Skip (Seq (compile (prog_using_Suc h)) (DoBinOp (op +)))" in exI)
  apply(rule conjI)
   apply(rule sems.intros(4))
   apply(blast intro: sems.intros)
  apply(rule_tac x="0#s" in exI)
  apply(rule_tac x="Seq (compile (prog_using_Suc h)) (DoBinOp op +)" in exI)
  apply(rule conjI)
   apply(rule sems.intros(5))
  apply(rule semsn_Seq_unchanged)
  apply blast
  done
  
lemma prog_using_Suc_not_stack_bound:
  "\<not> stack_bound (compile (prog_using_Suc h)) h"
  apply(simp add: stack_bound_def)
  using prog_using_Suc_uses_Suc not_le by metis 
  
lemma compile_has_no_universal_stack_bound:
  "\<not> (\<exists>h. (\<forall>p. stack_bound (compile p) h))"
  apply(rule notI)
  apply(erule exE)
  apply(drule_tac x="prog_using_Suc h" in spec)
  using prog_using_Suc_not_stack_bound by blast
 

(****************************************)
(* PART 2: "Rewriting rules for groups" *)
(****************************************)
  

(* Replace A-H below by equation stating that e is left- and right-neutral,
   and that i is left- and right-inverse, for the \<star> operator.
  
   Justify why your set of rules is confluent and terminating.
   (why it is safe to add them to the simp set)
*)

axiomatization
  e:: 'a and
  op:: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<star>" 70)  and
  i:: "'a \<Rightarrow> 'a" 
where
  neutral_left[simp]: "e \<star> x = x" and
  neutral_right[simp]: "x \<star> e = x" and
  inverse_left[simp]: "(i x) \<star> x = e" and
  inverse_right[simp]: "x \<star> (i x) = e"

(* This is terminating, but not confluent yet: *)
lemma critical_pair:
  "i e \<star> e = e"
  apply simp
  (* It should full simplify, but gets stuck -- this means it matters which rule was applied first. *)
  oops

(* If we take a different rule first, it works: *)
lemma critical_pair:
  "i e \<star> e = e"
  by (simp only: inverse_left)

(* The solution is complete the set by proving the reduct of the first lemma and add it as a simp
   rule *)
lemma critical_pair_join [simp]: "i e = e"
  using inverse_left [of e] by simp

(* Termination:
  Each application of the rewrite rules reduces the sum of the occurrences of 
  the operators "i" and "\<star>". Since the template suggested that the first four
  rules are sufficient for confluence, it was Ok to argue that each rule application
  reduces the number of \<star> in the formula.

  Confluence:
  Since we know termination, it is sufficient to show local confluence to get
  full confluence. We show local confluence by first investigating all lhs-pairs of the
  initial four rules and checking for critical pairs (overlapping unification):

    * (1) and (2): critical pair, but reduces to same result
    * (1) and (3): no critical pair
    * (1) and (4): i e = e (problem pair, use for completion)
    * (2) and (3): i e = e (same as above)
    * (2) and (4): no critical pair
    * (3) and (4): no critical pair

  The critical pair from (1) and (4) or (2) and (3) (essentially the same,
  just different direction), leads to our new rule critical_pair_join. We need
  to check for critical pairs of this new rule with all other rules. There are no
  such critical pairs. This means, we now have a terminating and locally confluent
  set of rules, and thereby a terminating and confluent one.
*)

end