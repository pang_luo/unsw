theory a2
imports Main
begin


(**********************************************)
(* PART 1: "Compiling Arithmetic Expressions" *)
(**********************************************)


(* Syntax of the language *)
datatype expr = BinOp "(nat \<Rightarrow> nat \<Rightarrow> nat)" expr expr |
                UnOp "(nat \<Rightarrow> nat)" expr |
                Const nat

definition
  plus :: "nat \<Rightarrow> nat \<Rightarrow> nat"
where
  "plus \<equiv> (\<lambda>a b. a + b)"
  
lemma "plus = (op +)"
  by(simp add: plus_def)

(* Evaluation function *)
primrec
  eval :: "expr \<Rightarrow> nat"
where
  "eval (Const n) = n" |
  "eval (BinOp f a b) = f (eval a) (eval b)" |
  "eval (UnOp f a) = f (eval a)"

lemma "eval (BinOp (op +) (Const 2) (Const 3)) = 5"
  by simp

(* Programs for the stack machine *)
datatype stackp = Push nat |
                  DoUnOp "nat \<Rightarrow> nat" |
                  DoBinOp "nat \<Rightarrow> nat \<Rightarrow> nat" |
                  Seq stackp stackp ("_ ;; _")  |
                  Skip

type_synonym stack = "nat list"

(* Big step operational semantics to programs for the stack machine *)
inductive
  sem :: "stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> bool" ("\<langle>_, _\<rangle> \<Down> _")
where
  sem_Push: "sem s (Push n) (n#s)" |
  sem_DoBinOp: "sem (a#b#s) (DoBinOp f) ((f a b)#s)" |
  sem_DoUnOp: "sem (a#s) (DoUnOp f) ((f a)#s)" |
  sem_Seq: "\<lbrakk>sem s a t; sem t b u\<rbrakk> \<Longrightarrow> sem s (Seq a b) u"

(* Simple compiler from expression to stack programs *)
primrec
  compile  :: "expr \<Rightarrow> stackp" 
where
  "compile (Const n) = Push n" |
  "compile (BinOp f a b) = Seq (compile b) (Seq (compile a) (DoBinOp f))" |
  "compile (UnOp f a) = Seq (compile a) (DoUnOp f)"

(* ---------- *)
(* Question 1 *)
(* ---------- *)

(* (a) Prove that the stack machine semantics is deterministic *)
inductive_cases PushE[elim!]: "\<langle>s, Push n\<rangle> \<Down> t"
inductive_cases DoBinOpE[elim!]: "\<langle>s, DoBinOp f\<rangle> \<Down> t"
inductive_cases DoUnOpE[elim!]: "\<langle>s, DoUnOp f\<rangle> \<Down> t"
inductive_cases SeqE[elim!]: "\<langle>s, a;;b\<rangle> \<Down> t"
inductive_cases SkipE[elim!]: "\<langle>s, Skip\<rangle> \<Down> t"

declare sem.intros [intro, simp]

lemma sem_det:
  "sem s e t \<Longrightarrow> sem s e u \<Longrightarrow> u = t"
  by (induction arbitrary: u rule: sem.induct) blast+

(* (b) Prove that the compiler is correct *)
lemma compile_correct:
  "sem s (compile e) ((eval e)#s)"
  apply(induct e arbitrary: s)
    apply auto
  done

(* (c) Prove that whether an expression can evaluate 
       depends only on the size of the initial stack *)
lemma helper_sem_stack_content_agnostic:
  "sem s p t \<Longrightarrow> \<forall>s'. length s' = length s \<longrightarrow> (\<exists>t'. sem s' p t' \<and> (length t' = length t))"
  apply(induction rule: sem.induct)
     apply auto
    apply(case_tac s')
     apply clarsimp
    apply(rename_tac a list)
    apply(case_tac list)
     apply auto[2]
   apply(case_tac s')
    apply auto[2]
  apply(erule_tac x = s' in allE)
  apply clarsimp
  apply(rename_tac t')
  apply(erule_tac x = t' in allE)
  apply auto
  done

lemma sem_stack_content_agnostic:
  "sem s p t \<Longrightarrow> \<forall>s'. length s' = length s \<longrightarrow> (\<exists>t'. sem s' p t')" 
  apply(frule helper_sem_stack_content_agnostic)
  apply blast
  done

  
(* ---------- *)
(* Question 2 *)
(* ---------- *)

(* Sufficient initial stack size *)
definition
  reqd_init_stack :: "stackp \<Rightarrow> nat \<Rightarrow> bool"
where
  "reqd_init_stack p h \<equiv> (\<exists>s t. \<langle>s,p\<rangle> \<Down> t) \<longrightarrow> (\<forall>s. length s \<ge> h \<longrightarrow> (\<exists>t. \<langle>s,p\<rangle> \<Down> t))"

(* (a) Prove that compiled expressions require no initial stack *)
lemma helper_compile_reqd_init_stack: "\<forall>s. \<exists>x. \<langle>s, compile e\<rangle> \<Down> (x#s)"
  apply(induct e)
    apply auto
   apply(rename_tac x1 e1 e2 s)
   apply(erule_tac x = s in allE, erule exE,
      erule_tac x = "x # s" in allE, erule exE,
      rule_tac x = "x1 xa x" in exI, erule sem_Seq, erule sem_Seq)
   apply clarsimp
  apply(erule_tac x = s in allE)
  apply auto
  done

lemma compile_reqd_init_stack:
  "reqd_init_stack (compile e) 0"
  apply(unfold reqd_init_stack_def)
  apply auto
  apply(insert helper_compile_reqd_init_stack)
  apply blast
  done

(* (b) Minimal initial stack length for atomic programs *)
lemma reqd_init_stack_Push:
  "reqd_init_stack (Push n) 0"
  apply(simp add: reqd_init_stack_def)
  apply auto
  done

lemma reqd_init_stack_DoUnOp:
  "reqd_init_stack (DoUnOp f) (Suc 0)"
  apply(simp add: reqd_init_stack_def)
  apply auto
  apply(case_tac sb)
   apply auto
  done

lemma reqd_init_stack_DoBinOp:
  "reqd_init_stack (DoBinOp f) (Suc (Suc 0))"
  apply(auto simp: reqd_init_stack_def)
  apply(rename_tac sb)
  apply(case_tac sb)
   apply simp
  apply(rename_tac list)
  apply(case_tac list)
   apply auto
  done

lemma reqd_init_stack_Skip:
  "reqd_init_stack Skip 0"
  apply(simp add: reqd_init_stack_def)
  apply auto
  done

(* (c) Minimal initial stack length for Seq *)
lemma helper_reqd_init_stack_Seq: "sem s p t \<Longrightarrow> sem (s @ xs) p (t @ xs)"
  apply(induction rule: sem.induct)
     apply auto
  done

lemma reqd_init_stack_Seq:
  "\<lbrakk>(reqd_init_stack p1) n; (reqd_init_stack p2 m)\<rbrakk>
       \<Longrightarrow> (reqd_init_stack (Seq p1 p2) (n + m))"
  apply(simp add: reqd_init_stack_def)
  apply auto
  apply(rename_tac s)
  apply(erule_tac x = "take n s" in allE)
  apply clarsimp
  apply(rename_tac x)
  apply(erule_tac x = "x @ drop n s" in allE)
  apply auto[1]
  apply(rename_tac tb)
  apply(rule_tac x = tb in exI)
  apply(drule_tac xs = "drop n s" in helper_reqd_init_stack_Seq, simp)
  done

(* (d) Define a function that given a program p 
       calculates an appropriate stack length h
       such that reqd_init_stack p h holds *)

primrec
  sufficient_init_stack :: "stackp \<Rightarrow> nat"
where
  "sufficient_init_stack (Push n) = 0" |
  "sufficient_init_stack (DoUnOp f) = Suc 0" |
  "sufficient_init_stack (DoBinOp f) = Suc (Suc 0)" |
  "sufficient_init_stack Skip = 0" |
  "sufficient_init_stack (Seq a b) = sufficient_init_stack a + sufficient_init_stack b"

(* (e) Prove your function from (d) correct *)
lemma helper_sufficient_init_stack_is_reqd_init_stack:
  "(\<langle>s, p\<rangle> \<Down> t) \<Longrightarrow> length s  \<le> length t + sufficient_init_stack p"
  apply(induct p arbitrary: s t)
      apply auto
  apply(rename_tac ta)
  apply(erule_tac x = s in meta_allE)
  apply(erule_tac x = ta in meta_allE)
  apply(erule_tac x = ta in meta_allE)
  apply(erule_tac x = t in meta_allE)
  apply simp
  done

lemma sufficient_init_stack_is_reqd_init_stack:
  "reqd_init_stack p (sufficient_init_stack p)"
  apply(induct p)
      apply auto
      apply(rule reqd_init_stack_Push)
     apply(rule reqd_init_stack_DoUnOp)
    apply(rule reqd_init_stack_DoBinOp)
   apply(simp add: reqd_init_stack_def)
   apply(rule impI)
   apply(rule allI)
   apply auto[2]
   apply(erule_tac x = s in allE)
   apply clarsimp
   apply(rename_tac p1 p2 s sa t ta x)
   apply(frule_tac s = s and p = p1 and t = x in helper_sufficient_init_stack_is_reqd_init_stack)
   apply(erule_tac x = x in allE)
   apply auto[1]
  apply(rule reqd_init_stack_Skip)
  done

(* ---------- *)
(* Question 3 *)
(* ---------- *)

(*  Small-step semantics *)
inductive
  sems :: "stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> bool"
where
  "sems s (Push n) (n#s) Skip" |
  "sems (a#b#s) (DoBinOp f) ((f a b)#s) Skip" |
  "sems (a#s) (DoUnOp f) ((f a)#s) Skip" |
  "sems s a s' a' \<Longrightarrow> sems s (Seq a b) s' (Seq a' b)" |
  "sems s (Seq Skip b) s b"

(* (a) Define a function semsn:: nat \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> bool
       that executes n steps of the small-step semantics *)
primrec
  semsn :: "nat \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> stack \<Rightarrow> stackp \<Rightarrow> bool"
where
  "semsn 0 s a s' a' = (s = s' \<and> a = a')" |
  "semsn (Suc n) s a s' a' = (\<exists>t b. sems s a t b \<and> semsn n t b s' a')"

(* (b) Prove that if a program a executes in the big-step semantics to a 
       resulting stack t from an initial stack s, then it executes in the
       small-step semantics to the same resulting stack and the resulting
       program Skip. *)
declare sems.intros [simp, intro]

lemma helper_semsn_correct [intro]:
  "semsn n s1 a1 s2 Skip
    \<Longrightarrow> semsn m s2 a2 s3 Skip
    \<Longrightarrow> semsn (n+m+1) s1 (a1;;a2) s3 Skip"
  apply(induct n arbitrary: s1 s2 s3 a1 a2)
   apply auto
  apply(rename_tac t b)
  apply(erule_tac x = t in meta_allE)
  apply(erule_tac x = s2 in meta_allE)
  apply(erule_tac x = s3 in meta_allE)
  apply(erule_tac x = b in meta_allE)
  apply(erule_tac x = a2 in meta_allE)
  apply auto
  done

lemma semsn_correct:
  "sem s a t \<Longrightarrow> \<exists>n. semsn n s a t Skip"
  apply(induct a arbitrary: s t)
      apply(rule_tac x = 1 in exI)
      apply auto[1]
     apply(rule_tac x = 1 in exI)
     apply auto[1]
    apply(rule_tac x = 1 in exI)
    apply auto[2]
   apply(rename_tac ta)
   apply(erule_tac x = s in meta_allE)
   apply(erule_tac x = ta in meta_allE)
   apply(erule_tac x = ta in meta_allE)
   apply(erule_tac x = t in meta_allE)
   apply auto
  done

(* (c) Prove that there is no universal stack bound for any compiled program *)
(* Predicate stating that stack size h is a stack bound for program p *)
definition
  stack_bound :: "stackp \<Rightarrow> nat \<Rightarrow> bool"
where
  "stack_bound p h \<equiv> \<forall>s n s' p'. semsn n s p s' p' \<longrightarrow>  length s' - length s \<le> h"

primrec
  prog_using_Suc :: "nat \<Rightarrow> expr"
where
  "prog_using_Suc 0 = Const 0" |
  "prog_using_Suc (Suc n) = (BinOp (op +) (prog_using_Suc n) (Const 0))"

lemma helper1_compile_has_no_universal_stack_bound [intro]: 
  "semsn n s a s' a' \<Longrightarrow> semsn n s (a ;; b) s' (a' ;; b)"
  apply(induct n arbitrary: s s' a a' b)
   apply auto
  apply(rename_tac b t ba)
  apply(rule_tac x = t in exI)
  apply(rule_tac x = "ba ;; b" in exI)
  apply auto
  done

lemma helper2_compile_has_no_universal_stack_bound [simp]: 
  "0 # replicate h 0 @ 0 # s = 0 # 0 # replicate h 0 @ s"
  by (simp add: replicate_append_same)

lemma helper3_compile_has_no_universal_stack_bound:
  "\<exists>a. semsn (2*h+1) s (compile (prog_using_Suc h)) (replicate (h+1) 0 @ s) a"
  apply(induct h arbitrary: s)
   apply(rule_tac x = Skip in exI)
   apply auto
  apply(erule_tac x = "0#s" in meta_allE)
  apply(erule exE)
  apply(rule_tac x = "x ;; (DoBinOp op +)" in exI)
  apply clarsimp
  apply(rule_tac x = "0#s" in exI)
  apply(rule_tac x = "Skip ;; ((compile (prog_using_Suc h)) ;; (DoBinOp op +))" in exI)
  apply clarsimp
  apply(rule_tac x = "0#s" in exI)
  apply(rule_tac x = "((compile (prog_using_Suc h)) ;; (DoBinOp op +))" in exI)
  apply auto
  done

lemma compile_has_no_universal_stack_bound:
  "\<not> (\<exists>h. (\<forall>p. stack_bound (compile p) h))"
  apply auto
  apply(rule_tac x = "prog_using_Suc h" in exI)
  apply(simp add: stack_bound_def)
  apply(insert helper3_compile_has_no_universal_stack_bound)
  apply(erule_tac x = h in meta_allE)
  apply(erule_tac x = s in meta_allE)
  apply(erule exE)
  apply(rule_tac x = s in exI)
  apply(rule_tac x = "2 * h + 1" in exI)
  apply(rule_tac x = "replicate (h + 1) 0 @ s" in exI)
  apply(rule conjI)
   apply(rule_tac x = a in exI)
   apply auto
  done

(****************************************)
(* PART 2: "Rewriting rules for groups" *)
(****************************************)
  

(* Replace A-H below by equation stating that e is left- and right-neutral,
   and that i is left- and right-inverse, for the \<star> operator.
  
   Justify why your set of rules is confluent and terminating.
   (why it is safe to add them to the simp set)
*)

axiomatization
  e:: 'a and
  op:: "'a \<Rightarrow> 'a \<Rightarrow> 'a" (infix "\<star>" 70)  and
  i:: "'a \<Rightarrow> 'a" 
where
  neutral_left[simp]: "e \<star> x = x" and
  neutral_right[simp]: "x \<star> e = x" and
  inverse_left[simp]: "(i x) \<star> x = e" and
  inverse_right[simp]: "x \<star> (i x) = e"

(*
The rules above terminate because there is a well founded order <\<^sub>r on terms
for which s <\<^sub>r t whenever t = s, where s <\<^sub>r t iff size(s) < size(t) with 
size(s) equal to the number of function symbols in s. In this case, the 
first two rules decrease size by 1 while the last two by 2 and <\<^sub>r is well 
founded since < is well founded on natural numbers.

Furthermore, there is no critical pair for the above rules, leading to local
confluence. Based on the fact that local confluence and termination yields
confluence, the set of rules above is confluent.
*)

end