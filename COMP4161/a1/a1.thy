theory a1
imports Main
begin

section "Question 1: Lambda-Calculus"

text {* Please refer to assignment1.pdf *}

section "Question 2: Higher Order Unification"

text {* Please refer to assignment1.pdf *}

section "Question 3: Propositional Logic"

lemma prop_a:
  "B \<longrightarrow> (B \<or> A)"
  apply(rule impI)
  apply(rule disjI1)
  apply assumption
  done

lemma prop_b:
  "(A = True) = A"
  apply(rule iffI)
  apply(erule iffE)
  apply(erule impE)+
  apply(rule TrueI)
  apply assumption
  apply(erule impE)
  apply assumption+
  apply(rule iffI)
  apply(rule TrueI)
  apply assumption
  done

lemma prop_c:
  "(A = False) = (\<not> A)"
  apply(rule iffI)
  apply(erule iffE)
  apply(rule notI)
  apply(erule impE)
  apply assumption+
  apply(rule iffI)
  apply(erule notE)
  apply assumption
  apply(erule FalseE)
  done

lemma prop_d: "P \<longrightarrow> \<not>\<not>P"
  apply(rule impI)
  apply(rule notI)
  apply(erule notE)
  apply assumption
  done

text {* This is provable only in a classical logic. *}
lemma prop_e: "\<not>\<not>P \<longrightarrow> P"
  apply(rule impI)
  apply(rule classical)
  apply(erule notE)
  apply assumption
  done

text {* 
Which of the above statements are provable only in a classical logic? 

Statement e is provable only in a classical logic.
*}

section "Question 4: Higher Order Logic"

lemma hol_a:
  "(\<forall>x. P x) \<or> (\<forall>x. Q x) \<longrightarrow> (\<forall>x. P x \<or> Q x)"
  apply(rule impI)
  apply(rule allI)
  apply(erule disjE)
  apply(rule disjI1)
  apply(erule_tac x = x in allE)
  apply(assumption)
  apply(rule disjI2)
  apply(erule_tac x = x in allE)
  apply assumption
  done

lemma hol_b:
  "(\<forall>P. P) = False"
  apply(rule iffI)
  apply(erule_tac x = False in allE)
  apply assumption
  apply(erule FalseE)
  done

lemma hol_c:
  "(\<forall>x. Q x = P x) \<and> ((\<exists>x. P x) \<longrightarrow> C) \<and> (\<exists>x. Q x) \<longrightarrow> C"
  apply(rule impI)
  apply(erule conjE)+
  apply(erule exE)
  apply(erule impE)
  apply(rule_tac x = x in exI)
  apply(erule_tac x = x in allE)
  apply(erule iffE)
  apply(erule impE)
  apply assumption+
  done
  
lemma hol_d:
  "(\<forall>x. \<not> (R x) \<longrightarrow> R (M x)) \<Longrightarrow> (\<forall>x. \<not> R (M x) \<longrightarrow> R x)"
  apply(rule allI)
  apply(rule impI)
  apply(erule_tac x = x in allE)
  apply(rule classical)
  apply(erule notE)
  apply(erule impE)
  apply assumption+
  done

lemma hol_e:
  "\<lbrakk>(\<forall>x. \<not> (R x) \<longrightarrow> R (M x)); (\<exists>x. R x)\<rbrakk> \<Longrightarrow> (\<exists>x. R x \<and> R (M (M x)))"
  apply(erule exE)
  apply(case_tac "R (M (M x))")
  apply(rule_tac x = x in exI)
  apply(rule conjI)
  apply assumption+
  apply(rule_tac x = "M x" in exI)
  apply(rule conjI)
  apply(erule_tac x = "M x" in allE)
  apply(rule classical)
  apply(erule notE)
  apply(erule impE)
  apply assumption+
  apply(erule_tac x = "M (M x)" in allE)
  apply(erule impE)
  apply assumption+
  done

  
text {* 
Formalise and prove the following statement using only the proof methods and rules as earlier in this question.

If every poor person has a rich mother, then there is a rich person
with a rich grandmother.
*}

text {*
We can formalise the above statement as lemma "statement", where (F x) and (M x) stand for x's 
father and mother respectively. Before we prove it, we introduce a lemma called "helper" which
assumes there is a rich person x, and then we prove the general case where we don't know whether
there exists a rich person in advance.
*}

lemma statement:
  "\<forall>x. \<not> (R x) \<longrightarrow> R (M x) \<Longrightarrow> \<exists>x. R x \<and> (R (M (M x)) \<or> R (M (F x)))"
  oops

lemma helper:
  "\<forall>x. \<not> (R x) \<longrightarrow> R (M x) \<Longrightarrow> R x \<Longrightarrow>  \<exists>x. R x \<and> (R (M (M x)) \<or> R (M (F x)))"
  apply(case_tac "R (M (M x))")
  apply(rule_tac x = x in exI)
  apply(rule conjI)
  apply assumption
  apply(rule disjI1)
  apply assumption
  apply(rule_tac x = "M x" in exI)
  apply(rule conjI)
  apply(erule_tac x = "M x" in allE)
  apply(rule classical)
  apply(erule notE)
  apply(erule impE)
  apply assumption+
  apply(rule disjI1)
  apply(erule_tac x = "M (M x)" in allE)
  apply(erule impE)
  apply assumption+
  done

lemma statement:
  "\<forall>x. \<not> (R x) \<longrightarrow> R (M x) \<Longrightarrow> \<exists>x. R x \<and> (R (M (M x)) \<or> R (M (F x)))"
  apply(case_tac "R x")
  apply(rule_tac x = x in helper)
  apply assumption+
  apply(case_tac "R (M x)")
  apply(rule_tac x = "M x" in helper)
  apply assumption+
  apply(erule_tac x = x in allE)
  apply(erule impE)
  apply assumption
  apply(erule notE)+
  apply assumption
  done
  
end
