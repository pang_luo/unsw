theory s1
imports Main
begin


lemma prop_a: "B \<longrightarrow> (B \<or> A)"
  apply (rule impI)
  apply (rule disjI1)
  apply (assumption)
  done

lemma prop_b:
  "(A = True) = A"
  apply(rule iffI)
   apply(erule iffE)
   apply(erule impE, rule TrueI)
   apply assumption
  apply(rule iffI)
   apply(rule TrueI)
  apply assumption
  done

lemma prop_c:
  "(A = False) = (\<not> A)"
  apply(rule iffI)
   apply(rule notI)
   apply(erule iffE)
   apply(erule impE, assumption)
   apply assumption
  apply(rule iffI)
   apply(erule notE)
   apply assumption
  apply (erule FalseE)
  done

lemma prop_d: "P \<longrightarrow> \<not>\<not>P"
  apply(rule impI)
  apply(rule notI)
  apply(erule notE)
  apply assumption
  done

text {* Provable only in a classical logic. *}    
lemma prop_e: "\<not>\<not>P \<longrightarrow> P"
  apply(rule impI)
  apply(rule ccontr)
  apply(erule notE)
  apply assumption
  done

lemma pred_a: "(\<forall>x. P x) \<or> (\<forall>x. Q x) \<longrightarrow> (\<forall>x. P x \<or> Q x)"
  apply(rule impI)
  apply(rule allI)
  apply(erule disjE)
   apply(erule allE)
   apply(rule disjI1)
   apply assumption
  apply(erule allE)
  apply(rule disjI2)
  apply assumption
  done


lemma pred_b: "(\<forall>P. P) = False"
  apply(rule iffI)
   apply(erule_tac x=False in allE)
   apply assumption
  apply(erule FalseE)
  done


lemma pred_c: "(\<forall>x. Q x = P x) \<and> ((\<exists>x. P x) \<longrightarrow> C) \<and> (\<exists>x. Q x) \<longrightarrow> C"
  apply(rule impI)
  apply(erule conjE)
  apply(erule conjE)
  apply(erule exE)
  apply(erule impE)
   apply(erule_tac x=x in allE)
   apply(erule iffE)
   apply(erule impE)
    apply assumption
   apply(rule_tac x=x in exI)
   apply assumption
  apply assumption
  done

lemma poor_mothers_have_rich_children:
  "(\<forall>x. \<not> (R x) \<longrightarrow> R (M x)) \<Longrightarrow> (\<forall>x. \<not> R (M x) \<longrightarrow> R x)"
  apply(rule allI)
  apply(rule impI)
  apply(case_tac "R x")
   apply assumption
  apply(erule_tac x=x in allE)
  apply(erule impE)
   apply assumption
  apply(erule notE)
   apply assumption
  done
  

lemma rich_grandmothers_helper:
  "\<lbrakk>(\<forall>x. \<not> (R x) \<longrightarrow> R (M x)); (\<exists>x. R x)\<rbrakk> \<Longrightarrow> (\<exists>x. R x \<and> R (M (M x)))"
  apply(erule exE, rename_tac some_guy)
   apply(case_tac "R (M (M some_guy))")
    apply(rule_tac x=some_guy in exI)
    apply(rule conjI)
     apply assumption
    apply assumption
   apply(rule_tac x="M some_guy" in exI)
   apply(rule conjI)
    apply(drule poor_mothers_have_rich_children)
    apply(erule_tac x="M some_guy" in allE)
    apply(erule impE)
     apply assumption
    apply assumption
   apply(erule_tac x="M (M some_guy)" in allE)
    apply(erule impE)
     apply assumption
    apply assumption
  done

lemma rich_grandmothers:
  "(\<forall>x. \<not> (R x) \<longrightarrow> R (M x)) \<longrightarrow> (\<exists>x. R x \<and> R (M (M x)))"
  apply(rule impI)
  apply(rule rich_grandmothers_helper)
   apply assumption
  apply(case_tac "R x")
   apply(rule_tac x=x in exI)
   apply assumption
  apply(erule_tac x=x in allE)
  apply(erule impE)
   apply assumption
  apply(rule_tac x="M x" in exI)
  apply assumption
  done


end
